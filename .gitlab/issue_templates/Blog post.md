<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. Unless you are just suggesting a blog post idea, you will need to create the formatted merge request for your blog post in order for it to be reviewed and published. -->

### Proposal

<!-- What do you want to blog about? Add your description here -->

### Checklist

- [ ] If you have a specific publish date in mind
  - [ ] Include it in the issue title
  - [ ] Give the issue a due date of a minimum of 2 working days prior
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts-official-announcements-company-updates-breaking-changes-and-news)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP

/label ~"blog post"
