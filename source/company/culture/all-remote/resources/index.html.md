--- 
layout: markdown_page
title: "Remote-work resources"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Resources on GitLab's all-remote approach

#### Articles

- [The office of the future is no office at all, says startup - Wall Street Journal](https://www.wsj.com/articles/the-office-of-the-future-is-no-office-at-all-says-startup-11557912601 )
- [8 secrets of GitLab's remote work culture](https://www.tfir.io/2019/05/15/8-secrets-of-gitlabs-remote-work-success/)
- [How to keep healthy communication habits in remote teams](https://medium.com/gitlab-magazine/how-to-keep-healthy-communication-habits-in-remote-teams-a19eca371952)
- [Remote work demands a supportive company culture](https://insights.dice.com/2019/01/22/remote-work-demands-supportive-company-culture/)
- [5 fun strategies companies are using to make remote workers feel included](https://business.linkedin.com/talent-solutions/blog/employee-engagement/2019/strategies-companies-use-to-keep-remote-workers-feeling-included)
- [Tech's newest unicorn has employees in 45 countries and zero offices](https://qz.com/work/1394496/gitlab-techs-newest-unicorn-has-no-offices/)
- [No need to come to the office: Making remote work at GitLab](https://hackernoon.com/no-need-to-come-to-the-office-making-remote-work-at-gitlab-737c42865210)
- [GitLab and Buffer CEOs talk transparency at scale](https://about.gitlab.com/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/)
- [Why GitLab is remote only](http://tapes.scalevp.com/remote-only-gitlab-sytse-sid-sijbrandij/)

#### Blogs from GitLab team members

- [How we turned a dull weekly all-hands into a podcast](https://about.gitlab.com/2019/06/03/how-we-turned-40-person-meeting-into-a-podcast/)
- [The GitLab handbook by the numbers](https://about.gitlab.com/2019/04/24/the-gitlab-handbook-by-numbers/)
- [Remote manifesto by GitLab](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
- [Working remotely at GitLab: an update](https://about.gitlab.com/2016/03/04/remote-working-gitlab/) 
- [The case for all-remote companies](https://about.gitlab.com/2018/10/18/the-case-for-all-remote-companies/)
- [How remote work enables rapid innovation at GitLab](https://about.gitlab.com/2019/02/27/remote-enables-innovation/)
- [Why GitLab pays local rates](https://about.gitlab.com/2019/02/28/why-we-pay-local-rates/)
- [Remote work: 9 tips for eliminating distractions and getting things done](https://about.gitlab.com/2018/05/17/eliminating-distractions-and-getting-things-done/)
- [A day in the life of a remote Sales Development Representative](https://about.gitlab.com/2018/05/11/day-in-life-of-remote-sdr/)
- [The remote future: How tech companies are helping their remote teams connect](https://about.gitlab.com/2018/04/27/remote-future-how-remote-companies-stay-connected/)
- [Transition to remote work - one month in](https://www.linkedin.com/pulse/transition-remote-work-1-month-casey-allen-shobe/)
- [Remote work, done right](https://about.gitlab.com/2018/03/16/remote-work-done-right/)
- [How working at GitLab has changed my view on work and life](https://about.gitlab.com/2018/03/15/working-at-gitlab-affects-my-life/) 
- [GitLab team-members share how to recognize burnout and how to prevent it](https://about.gitlab.com/2018/03/08/preventing-burnout/)
- [How we stay connected as a remote company](https://about.gitlab.com/2016/12/05/how-we-stay-connected-as-a-remote-company/)
- [How to become the best distributed software development team](https://about.gitlab.com/2017/09/15/pick-your-brain-interview-kwan-lee/)
- [How to keep remote (volunteer) teams engaged](https://about.gitlab.com/2016/12/21/how-to-keep-remote-teams-engaged/)
- [3 things I learned in my first month at GitLab](https://about.gitlab.com/2016/11/02/three-things-i-learned-in-my-first-month-at-gitlab/)
- [Working at GitLab: 30 days later](https://about.gitlab.com/2016/11/03/working-at-gitlab-30-days-later/)
- [What it's like to be a parent working at GitLab](https://about.gitlab.com/2016/04/08/remote-working-parents/)
- [To work remotely you need: Wifi and good communication habits](https://about.gitlab.com/2016/03/23/remote-communication/)
- [Highlights to my first remote job](https://about.gitlab.com/2015/06/17/highlights-to-my-first-remote-job/)
- [I'm leaving GitLab to help everyone work remotely](https://medium.com/@jobv/im-leaving-gitlab-to-help-everyone-work-remotely-1f6828ec45d5)

#### Videos, interviews, presentations

- [How to build and manage a distributed team - GitLab and Arch Systems](https://youtu.be/tSp5se9BudA) 
- [Building a distributed company](https://outklip.com/blog/gitlab-building-a-distributed-company/)
- [Using video for effective collaboration for remote teams](https://youtu.be/6mZqzK_40FE)
- [GitLab’s secret to managing 160 employees in 160 locations - Interview by Y Combinator](https://blog.ycombinator.com/gitlab-distributed-startup/)
- [GitLab and Turtle's CEOs talk about running remote companies](https://youtu.be/-23DGyIMorE)
- [GitLab's remote-only presentation, 2017](https://docs.google.com/presentation/d/1JHHYQvAhsudGz8QB8nqp5ScJjqyhPD3ehCoKOlZb7VE/edit#slide=id.g1d6fee80ee_0_348)
- [Opening keynote from GitLab Contribute, 2019](https://youtu.be/kDfHy7cv96M?t=735)

#### Threads and conversations

- [HackerNews thread about all-remote page](https://news.ycombinator.com/item?id=19785262)
- [HackerNews thread about why more companies aren't remote](https://news.ycombinator.com/item?id=20104173)

## Resources on remote work 

#### Articles

- [How remote work can reduce stress and revitalize your mindset](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/)
- [The 1 trait all remote workers need -- here is how to cultivate it](https://www.inc.com/brian-de-haaff/the-one-trait-all-remote-workers-need-here-is-how-to-cultivate-it.html)
- [4 golden rules for living the laptop lifestyle](https://www.forbes.com/sites/stephanieburns/2019/06/14/4-golden-rules-for-living-the-laptop-lifestyle/)
- [5 ways to make your remote team more effective](https://www.forbes.com/sites/serenitygibbons/2019/06/13/5-ways-to-make-your-remote-team-more-effective/)
- [How to make remote work, work](https://www.inc.com/shama-hyder/how-to-make-remote-work-work.html)
- [6 ways in which offering flexible working makes you a great employer](https://www.business2community.com/human-resources/6-ways-in-which-offering-flexible-working-makes-you-a-great-employer-02209279)
- [Google spent 2 years researching what makes a great remote team](https://www.inc.com/justin-bariso/google-spent-2-years-researching-what-makes-a-great-remote-team-it-came-up-with-these-3-things.html)
- [5 reasons remote teams are more engaged than office workers](https://www.business.com/articles/remote-workers-more-engaged/)
- [Remote work becoming "new normal"](https://finance.yahoo.com/news/remote-working-becoming-normal-190509351.html)
- [Here's why remote workers are more productive than in-house teams](https://www.forbes.com/sites/abdullahimuhammed/2019/05/21/heres-why-remote-workers-are-more-productive-than-in-house-teams/)
- [5 ways to stay motivated when you work remote](https://www.forbes.com/sites/stephanieburns/2019/05/30/5-ways-to-stay-motivated-when-you-work-remote/)
- [Five mistakes that destroy the efficiency of your remote workers](https://www.forbes.com/sites/forbescoachescouncil/2019/05/29/five-mistakes-that-destroy-the-efficiency-of-your-remote-workers)
- [These Are the 8 Best MacOS Apps for Working Remotely](https://www.inc.com/jason-aten/these-are-8-best-macos-apps-for-working-remotely.html)
- [The benefits of and questions facing remote and distributed startups](https://tomtunguz.com/remote-and-fully-distributed/)
- [The end of an era for easyDNS](https://easydns.com/blog/2019/05/23/the-end-of-an-era-for-easydns/)
- [Engagement around the world, charterd - HBR](https://hbr.org/2019/05/engagement-around-the-world-charted)
- [5 Important Takeaways From Google's Two-Year Study Of Remote Work](https://www.forbes.com/sites/abdullahimuhammed/2019/05/18/5-important-takeaways-from-googles-two-year-study-of-remote-work/#56e705797439)
- [State of Remote Work 2019 by Buffer](https://buffer.com/state-of-remote-work-2019)
- [How to build a remote team](https://decryptmedia.com/7052/how-to-build-a-remote-team)
- [The IWG Global Workspace Survey - Welcome to Generation Flex](http://assets.regus.com/pdfs/iwg-workplace-survey/iwg-workplace-survey-2019.pdf)
- [Remote work doesn't scale... or does it?](https://medium.com/@getadam/remote-work-doesnt-scale-or-does-it-4a72ce2bb1f3)
- [Case Closed: Work-From-Home is the World's Smartest Management Strategy](https://www.inc.com/geoffrey-james/case-closed-work-from-home-is-worlds-smartest-management-strategy.html)
- [A Long Commute Could Be the Last Thing Your Marriage Needs](https://www.forbes.com/sites/markeghrari/2016/01/21/a-long-commute-could-be-the-last-thing-your-marriage-needs/#5baf10f04245)
- [After Growing to 50 People, We’re Ditching the Office Completely](https://open.buffer.com/no-office/)
- [On-Premise Tribes in Shiny Caves](https://medium.com/understanding-as-a-service-uaas/on-premise-people-and-shiny-caves-remote-as-a-service-97cff86382b6)
- [Remote working tips by Groove](https://www.groovehq.com/blog/remote-work-tips)
- [Being tired isn’t a badge of honor](https://m.signalvnoise.com/being-tired-isn-t-a-badge-of-honor-fa6d4c8cff4e)
- [The benefits - and pitfalls - of working in isolation](http://theconversation.com/the-benefits-and-pitfalls-of-working-in-isolation-105350)
- [It's not just the isolation. Working from home has surprising downsides](https://theconversation.com/its-not-just-the-isolation-working-from-home-has-surprising-downsides-107140)
- [The Day They Invented Offices](https://shift.infinite.red/a-hypothetical-conversation-with-a-real-estate-developer-in-a-world-without-offices-53cd7be0942#.pufgl7l3a)
- [Introverts at Work: Designing Spaces for People Who Hate Open-Plan Offices](http://www.bloomberg.com/news/articles/2014-06-16/open-plan-offices-for-people-who-hate-open-plan-offices)
- [Guidelines for Effective Collaboration](https://github.com/ride/collaboration-guides)
- [The Ultimate Guide to Remote Standups](http://blog.idonethis.com/ultimate-guide-remote-standups/)
- [How Do You Manage Global Virtual Teams?](https://en.wikibooks.org/wiki/Managing_Groups_and_Teams/How_Do_You_Manage_Global_Virtual_Teams%3F)
- [Getting Virtual Teams Right](https://hbr.org/2014/12/getting-virtual-teams-right)
- [Tweets about the impact by Amir Salihefendic](https://twitter.com/amix3k/status/881251640795439104)
- [Martin Fowler on remote vs. co-located](https://martinfowler.com/articles/remote-or-co-located.html)
- [That remote work think piece has some glaring omissions (a rant)](http://www.catehuston.com/blog/2016/04/07/that-remote-work-think-piece-has-some-glaring-omissions/)

#### Videos, interviews, presentations

- [Things I Wish I Knew Before Going Remote by Marla Brizel Zeschin](https://www.youtube.com/watch?v=nUQ41-vBtdg)
- [Bond Internet Trends 2019: Remote work = creating internet-enabled work opportunities + efficiencies](https://www.bondcap.com/report/itr19/#view/228)

#### Organizations for traveling remote work

- [Remote Year](http://www.remoteyear.com/)
- [Wifi Tribe](https://wifitribe.co/)
- [The Remote Experience](https://www.theremoteexperience.com/)
- [Hacker Paradise](https://www.hackerparadise.org/)
- [Wifly Nomads](https://www.wiflynomads.com/)
- [Co-work Paradise](http://www.coworkparadise.com/)
- [Project Getaway](http://www.projectgetaway.com/)
- [B-Digital Nomad](https://www.b-digitalnomad.com/)
- [Digital Outposts](http://digitaloutposts.com/)

#### Guides for remote work
- [Google's Distributed Work Playbook](http://services.google.com/fh/files/blogs/distributedworkplaybooks.pdf)
- [Remote Habits](https://remotehabits.com/)
- [35 of the best Slack communities for remote workers](https://www.owllabs.com/blog/remote-work-slack-communities?hs_amp=true)

#### Remote company aggregation sites

- [RemoteHub](https://remotehub.io/)
- [Remote In Tech](https://github.com/remoteintech/remote-jobs)

#### Job boards for remote employees

Here's a [list](https://www.ryrob.com/remote-jobs-websites/) of 60 remote jobs sites in 2019, including: 

- [We Work Remotely](https://weworkremotely.com)
- [Remote OK](https://remoteok.io)
- [Jobspresso](https://jobspresso.co/)
- [Working Nomads](http://www.workingnomads.co/jobs)
- [PowerToFly](https://powertofly.com)
- [Remote jobs on Angellist](https://angel.co/job-collections/remote)
- [Remotive](https://remotive.io/) - in particular, [The List of Awesome](https://docs.google.com/spreadsheets/d/1TLJSlNxCbwRNxy14Toe1PYwbCTY7h0CNHeer9J0VRzE/htmlview?pli=1#gid=0)
- [Who Is Hiring?](https://whoishiring.io/)
- [JustRemote](https://justremote.co)
- [Remote For Me](https://remote4me.com/)
- [FlexJobs](https://www.flexjobs.com/)
- [Remote.Co](https://remote.co/)


## Organizations leading the way

#### All-remote companies

- [InVision](https://www.invisionapp.com/), see their posts about [remote-only motivations](https://www.invisionapp.com/blog/studio-remote-design-team/), building [company culture](https://www.invisionapp.com/blog/remote-company-culture/), and what remote work [feels like](https://www.invisionapp.com/blog/remote-worker-truths/).
- [Buffer](https://buffer.com), see their posts about going [remote only](https://open.buffer.com/no-office/), [the benefits](https://open.buffer.com/distributed-team-benefits/), and how they [make it work](https://open.buffer.com/buffer-distributed-team-how-we-work/).
- [Automattic](https://automattic.com/about/), the company behind [WordPress.com](https://wordpress.com) and [The year without pants](https://www.amazon.com/Year-Without-Pants-WordPress-com-Future/dp/1118660633) fame
- [GitLab](https://about.gitlab.com), this website is hosted by GitLab, everyone is welcome to contribute to [this page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/index.html.md), and also check out [our handbook](https://about.gitlab.com/handbook/) for remote work practices.
- [Zapier](https://zapier.com/), wrote a [book on remote work](https://zapier.com/learn/remote-work/), is 100% remote.
- [Close.io](https://close.io), see their [jobs page](http://jobs.close.io/) and [blog post about remote culture](http://blog.close.io/remote-team-culture).
- [Groove](https://www.groovehq.com/), see their blog on being a [remote team](https://www.groovehq.com/blog/being-a-remote-team).
- [Anybox](https://anybox.fr/), see their feedback (in french) [Télétravail généralisé, notre retour d'expérience](https://anybox.fr/blog/teletravail-generalise-confiance-et-seminaires).
- [Screenly](https://www.screenly.io), this is [how they work](https://www.screenly.io/blog/2016/11/23/how-we-work-at-screenly/).
- [Soshace](https://soshace.com), see their [jobs page](https://soshace.com/careers).
- [Innolitics](http://innolitics.com), see their [handbook](http://innolitics.com/about/handbook/) which describes how they work remotely.
- [IOpipe](https://iopipe.com)
- [HiringThing](https://www.hiringthing.com/), see their post about what it is like to work [remote-only](https://www.hiringthing.com/2018/04/19/whats-it-like-to-work-at-hiringthing.html).
- [SerpApi](https://serpapi.com)
- [Scrapinghub](https://scrapinghub.com/), who turns websites into data. See their [about us page](https://scrapinghub.com/about), [open positions](https://scrapinghub.com/jobs), and [blog](https://blog.scrapinghub.com/).
- [Jitbit](https://www.jitbit.com/)
- [ProxyCrawl](https://proxycrawl.com)
- [Toptal](https://www.toptal.com/), see their Benefits: ["Fully Remote, No offices, no useless meetings, no mandatory hours. You’re recognized for what you do, not your time in a chair"](https://www.toptal.com/careers), also see [FAQ: "Where do Toptal experts work?"](https://www.toptal.com/faq#where-do-toptal-experts-work).
- [Sonatype](https://www.sonatype.com/), blog article on remote work at [Sonatype](https://blog.sonatype.com/the-magic-of-a-remote-organization).
- [Podia](https://www.podia.com), a remote-only company, see [their founder's Tweetstorm](https://twitter.com/spencerfry/)
- [Knack](https://www.knack.com), a remote-only company, see [why they work remotely](https://www.knack.com/blog/why-we-chose-remote).
- [Honeybadger](https://www.honeybadger.io)
- [Doist](https://doist.com), makers of [Todist](https://todoist.com) and [Twist](https://twistapp.com). Here's how they [make remote work happen](https://blog.doist.com/https-blog-doist-com-managing-remote-teams-622521189e80).
- [Discourse](https://www.discourse.org/), see Coding Horror's ["We hire the best, just like everyone else"](https://blog.codinghorror.com/we-hire-the-best-just-like-everyone-else/)
- [Aha!](https://www.aha.io/), see their blog about [remote work](https://blog.aha.io/category/remote-work/).
- [Articulate](https://articulate.com), see their [company page](https://articulate.com/company) for how they work.
- [Infinity Interactive](https://iinteractive.com/), see their [benefits page](https://iinteractive.com/benefits) for their embrace of remote working.
- [Altcoin Fantasy](https://altcoinfantasy.com/), see their [tips on hiring and retaining performant remote workers for an early startup](https://blog.altcoinfantasy.com/2018/06/13/tips-hiring-retaining-remote-employees-early-stage-startups).
- [Nozbe](https://nozbe.com), see [what they do](https://nozbe.com/about), [how they work](https://nozbe.com/blog/why-nooffice/) and [what are Nozbe’s values](https://nozbe.com/blog/nozbe-values/).
- [Duck Duck Go](https://duckduckgo.com/), see their ["Work Whenever, Wherever" Careers page](https://duckduckgo.com/hiring/), also see [Making Remote Work: Ask DuckDuckGo About Being A Digital Nomad Q&A](http://womendigitalnomads.com/blog/duckduckgo-digital-nomad/).
- [FormAssembly](http://www.formassembly.com/), see their [jobs page](https://formassembly.workable.com/). Also see their [blog post: "[Infographic] Benefits of Working Remotely"](https://www.formassembly.com/blog/remote-work-benefits/).
- [Gruntwork](https://gruntwork.io/): see [how they built a distributed, self-funded, family-friendly, profitable startup](https://blog.gruntwork.io/how-we-built-a-distributed-self-funded-family-friendly-profitable-startup-93635feb5ace).
- [Idea To Startup](https://ideatostartup.org/), see their [join page: _"2. Work From Anyplace. It doesn't matter whether you live"_ .... _"You can work from anyplace"_](https://ideatostartup.org/join/).
- [SoftwareMill](https://softwaremill.com/), see their [pros&cons of remote work post](https://softwaremill.com/remote-work-pros-and-cons/) & others
- [Pangian](https://pangian.com/) _"Top talent working remotely. Worldwide"_, see their [about page](https://mailchi.mp/pangian.com/remote-network) _"The fastest-growing remote talent network. Worldwide"_.
- [Winding Tree](https://windingtree.com/), Decentralized Travel Ecosystem built by decentralized team.
- [reinteractive](https://reinteractive.com/)
- [easyDNS](https://easydns.com/), see why they decided to [go all-remote.](https://easydns.com/blog/2019/05/23/the-end-of-an-era-for-easydns/)

#### Remote-first companies

- [Basecamp](https://basecamp.com/), authors of [Remote](https://37signals.com/remote)
- [Harvest](https://www.getharvest.com/), see their collection of stories on [Working without borders](https://www.getharvest.com/working-without-borders)
- [Nota](https://notainc.com), builders of a [modern idea hub](https://scrapbox.io) & [instant screenshot sharing app](https://gyazo.com) perfect for remote work.
- [Niteo](https://niteo.co/), a decade-old Python boutique with a public [handbook](https://github.com/niteoweb/handbook)
- [ElevenYellow](https://elevenyellow.com), type `job openings` on their [console](https://www.elevenyellow.com/) to join this team of digital nomads.
- [wemake.services](https://wemake.services), software development company using [`RSDP`](https://wemake.services/meta/)
- [Igalia](https://igalia.com), [employee-owned cooperative](https://www.igalia.com/about-us/great-place-to-work/) providing software development consulting services
- [Ad Hoc](https://adhocteam.us/), see their post ["The truth about remote work"](https://adhocteam.us/2017/12/05/truth-remote-work/) and their [jobs page](https://adhocteam.us/join/).

## Contribute to this page

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community. 
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.

----

Return to the main [all-remote page](/company/culture/all-remote/).

