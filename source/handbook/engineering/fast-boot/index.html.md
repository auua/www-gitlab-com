---
layout: markdown_page
title: Fast Boot
---

## On this page
{:.no_toc}

- TOC
{:toc}

A Fast Boot is an event that gathers the members of a team or group in one
physical location to work together and bond in order to accelerate the
formation of the team or group so that they reach maximum productivity as
early as possible.

## History of the Fast Boot

The first Fast Boot took place in December 2018. The 13 members of Monitor
Group gathered for 3 days to work and bond in Berlin. You can learn more
by reading the [original planning issue](https://gitlab.com/gitlab-com/organization/issues/185).

## Why should you have a Fast Boot?

Right now, the fast boot is intended for new teams or for teams with a majority
of new members who need to build their culture of shipping work. If your team
fits this description, you can propose holding a Fast Boot to reduce ramp up
time and establish and strengthen relationships between team members.

## How should you get approval?

First raise the idea with your manager to determine if there’s a good case
for a Fast Boot. Ultimately, the executive team will approve these on a case
by case basis.

### Development approval process and timelines

Because fast boots ultimately require a budget and approval, fast boots will be
limited by both factors. As of June 12, 2019 Engineering is limited to one fast
boot for FY20. As such, requests for fast boots need to be submitted by July 31,
2019 and will be prioritized. Approval will occur in by August 15, 2019. If
successful, more fast boots can be planned for the FY21 budget.

## How should you measure success?

When building the case for your Fast Boot, you should determine what
metrics you will use to measure success. For example, after the Fast Boot
you may expect your engineering team's throughput to increase or you may
expect your support teams mean time to ticket resolution to decrease. You
may also want to measure the sentiment of the team members after the Fast Boot
with followup surveys.

## What artifacts should you produce?

During the Fast Boot the priority is building a culture of shipping work.
Ideally the group can work together to ship one large, high-impact item to
production. Failing that it could be a backlog of smaller items. Secondarily you
should record and livestream videos that give people insight into what your team
is working on. Kickoff meetings, working sessions, and demos are all great
candidates for being livestreamed.

## Pros & Cons

### Pros

* Team members really enjoyed meeting each-other in person and got to spend
a lot more time together than at the Summit/Contribute
* Team members now feel more comfortable asking for help, especially across
teams (FE, BE, UX)

### Cons

* It takes a fair amount of planning: hotels, flights, meals, content,
follow-up
* It can be expensive
* It is antithetical to the way we usually work at GitLab

## Tips & Tricks

* Hold the Fast Boot in a location where at least one of your team members
resides. In addition to reducing the cost of flights and hotels, your team
member can act as a guide to the city and help choose and book locations.
* Set clear expectations about how your time will be spent. Will there be
down time? Will the team eat dinner together every night?

