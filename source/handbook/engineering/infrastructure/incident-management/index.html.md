---
layout: markdown_page
title: "Incident Management"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Incidents

Incidents are **anomalous conditions** that result in&mdash;or may lead
to&mdash;service degradation or outages. These events require human
intervention to avert disruptions or restore service to operational status.
Incidents are _always_ given immediate attention.

### Is GitLab.com Experiencing an Incident?

If you're observing issues on GitLab.com or working with users who are
reporting issues, please follow the instructions found on the
[On-Call](handbook/on-call/) page and alert the Engineer On Call (EOC).

If any of the dashboards below are showing major error rates or deviations,
it's best to alert the Engineer On Call.

#### Critical Dashboards

1. What alerts are going off? [Prometheus gprd](https://prometheus.gprd.gitlab.net/alerts#)
1. How does do these dashboards look?
  - [Triage dashboard](https://dashboards.gitlab.net/d/RZmbBr7mk/gitlab-triage?orgId=1&refresh=30s)
  - [General Triage dashboard](https://dashboards.gitlab.net/d/1UpWp7dik/general-triage?orgId=1)
    1. What services are showing availability issues?
    1. What components are outside of normal operations?
      * [Triage-components](https://dashboards.gitlab.net/d/VE4pXc1iz/general-triage-components?orgId=1)
      * [Triage-services](https://dashboards.gitlab.net/d/WOtyonOiz/general-triage-service?orgId=1)

## Incident Management

The goal of incident management is to organize chaos into swift incident
resolution. To that end, incident management provides:

- well-defined [roles and responsibilities](#roles-and-responsibilities) for members of the incident team,
- control points to manage the flow **information** and the **resolution path**,
- a **root-cause analysis** (RCA),
- and a post-incident review that assigns a **severity** classification after assessing the impact and scope of the incident.

### Ownership

There is only ever **one** owner of an incident&mdash;and only the owner of
the incident can declare an incident resolved. At anytime the incident owner
can engage the next role in the hierarchy for support. 

### Roles and Responsibilities

It's important to clearly delineate responsibilities during an incident.
Quick resolution requires focus and a clear hierarchy for delegation of
tasks. Preventing overlaps and ensuring a proper order of operations is vital
to mitigation. The responsibilities outlined in the roles below are
cascading–**and ownership of the incident passes from one role to the next as
those roles are engaged**. Until the next role in the hierarchy engages, the
previous role assumes all of the subsequent roles' responsibilities and
retains ownership of the incident.

| **Role** | **Responsibilities** |
| -------- | ------------------------|
| `EOC`    | **Engineer On Call** |
|          | The Production **Engineer On Call** is generally an SRE and can declare an incident. If another party has declared an incident, once the EOC is engaged they own the incident. The EOC gathers information, performs an initial assessment, and determines the [incident severity](#incident-severity) level. After the incident is resolved, the EOC is responsible for performing a [root-cause analysis](#root-cause-analysis-rca).|
| `IMOC`   | **Incident Manager On Call** |
|          | The **Incident Manager** is generally a Reliability Engineering manager and is engaged when incident resolution requires coordination from multiple parties. The IMOC is the tactical leader of the incident response team&mdash;not a person performing technical work. The IMOC assembles the Incident Team by engaging individuals with the skills and information required to resolve the incident. They evaluate information provided by team members, lend technical direction, and coordinate troubleshooting efforts. After the incident is resolved, the IMOC is responsible for conducting the [post-incident review](#post-incident-review).|
| `CMOC`   | **Communications Manager On Call** |
|          | The **Communications Manager** is generally a Reliability Engineering manager. The CMOC disseminates information internally to stakeholders and externally to customers across multiple media (e.g. GitLab issues, Twitter, status.gitlab.com, etc.). Once a severity S1 or S2 incident has persisted for longer than 30 minutes the CMOC must be engaged. After the incident is resolved, the CMOC is responsible for working with the Content Marketing team to determine if any additional external communications need to be produced.|

These definitions imply several on-call rotations for the different roles.

### Runbooks

[Runbooks](https://gitlab.com/gitlab-com/runbooks) are available for
engineers on call. The project README contains links to checklists for each
of the above roles.

### Declaring an Incident

The following steps can be automated in Slack by typing `/start-incident`. If the commend fails, manually do the following:
1. Create an issue with the label `Incident`, on the `production` queue with the template for [Incident](https://gitlab.com/gitlab-com/gl-infra/production/issues/new?issuable_template=incident) . If it is not possible to generate the issue, start with the tracking document and create the incident issue later.
1. Ensure the initial severity label is accurate.

Optional - not required for post deployment patches and as needed for the incident:
1. If S1/S2 outage, Create an issue with the label `~IncidentReview` on the `infrastructure` queue with the template for [RCA](
https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/new?issuable_template=rca). If it is not possible to generate the issue, start with the tracking document and create the incident issue later.
1. Create and associate a google doc with [the template](https://docs.google.com/document/d/1NMZllwnK70-WLUn_9IiiyMWeXs-JKPEiq-lordxJAig/edit#), to both issues, production and infrastructure. Populate and use the google doc as source of truth during the incident. This doc is mainly for real time multiple access when we cannot use the production issue for communication.
1. Contact the CMOC for support during the incident.

Issue [`infra/5543`](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/5543) tracks automation for incident management.

## Communication

Information is an asset to everyone impacted by an incident. Properly managing the flow of information is critical to minimizing surprise and setting expectations. We aim to keep interested stakeholders apprised of developments in a timely fashion so they can plan appropriately.

This flow is determined by:

* the type of information,
* its intended audience,
* and timing sensitivity.

Furthermore, avoiding information overload is necessary to keep every stakeholder’s focus.

To that end, we will have:

* a dedicated incident bridge (Zoom call) for all incidents.
* a Google Doc as needed for multiple user input based on the [shared template](https://docs.google.com/document/d/1NMZllwnK70-WLUn_9IiiyMWeXs-JKPEiq-lordxJAig/edit#)
* a dedicated `#incident-management` channel for internal updates
* regular updates to status.gitlab.com via status.io that disseminates to various media (e.g. Twitter)
* a dedicated repo for issues related to [Production](https://gitlab.com/gitlab-com/production) separate from the queue that holds Infrastructure's workload: namely, issues for incidents and changes.

### Status

We manage incident [communication](#communication) using status.io, which updates status.gitlab.com. Incidents in status.io have **state** and **status** and are updated by the incident owner.

Definitions and rules for transitioning state and status are as follows. 

| **State** | **Definition** |
| --------- | -------------- |
| Investigating | The incident has just been discovered and there is not yet a clear understanding of the impact or cause. If an incident remains in this state for longer than 30 minutes after the EOC has engaged, the incident should be escalated to the IMOC.|
| Identified | The cause of the incident is believed to have been identified and **a step to mitigate has been planned and agreed upon**. |
| Monitoring | The step has been executed and metrics are being watched to ensure that we're operating at a baseline  |
| Resolved | The incident is closed and status is again Operational.  |

Status can be set independent of state. The only time these must align is when an issues is 

| **Status** | **Definition** |
| ---------- | -------------- |
| Operational | The default status before an incident is opened and after an incident has been resolved. All systems are operating normally. |
| Degraded Performance | Users are impacted intermittently, but the impacts is not observed in metrics, nor reported, to be widespread or systemic. |
| Partial Service Disruption | Users are impacted at a rate that violates our SLO. The IMOC must be engaged and monitoring to resolution is required to last longer than 30 minutes. |
| Service Disruption | This is an outage. The IMOC must be engaged. |
| Security Issue | A security vulnerability has been declared public and the security team has asked to publish it. |

## Post-Incident

### Root Cause Analysis (RCA)

1. The owner of the RCA is the EOC. The EOC will fill the issue of `production` and `infrastructure` when the incident is mitigated, with the info from the tracking document.
1. When necessary, new tickets will be created with the label "Corrective Action" and linked with the RCA Issue on the infrastructure track.
1. Closing the RCA ~IncidentReview issue:  When discussion on the RCA issue is complete and all `~corrective action` issues have been linked, the issue can be closed.  The infrastructure team will have a cadence to review and prioritize corrective actions.

### Post-Incident Review

Every incident is assigned a severity level

## Severities

### Incident Severity

**Incident severities encapsulate the impact of an incident and scope the resources allocated to handle it**. Detailed definitions are provided for each severity, and these definitions are reevaluated as new circumstances become known. Incident management uses our standardized severity definitions, which can be found under our [issue workflow documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#severity-labels).

### Alert Severities

* Alerts severities do not necessarily determine incident severities. A single incident can trigger a number of alerts at various severities, but the determination of the incident's severity is driven by the above definitions.
* Over time, we aim to automate the determination of an incident's severity through service-level monitoring that can aggregate individual alerts against specific SLOs.
