---
layout: markdown_page
title: "Test Engineering Process"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Quality Engineering department helps facilitate the test planning process for all things related to Engineering work.

## Child Pages

#### [Test Design Heuristics](./test-design)

### Test Planning

The goal of our test planning process is to ensure quality and manage risk in an efficient and effective manner.

To achieve that goal when delivering a change or new feature, we have conversations that link test requirements to product requirements, identify risks and quality concerns, and review and plan test coverage across [all test levels](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html).

The output or deliverables of the test planning process are:
* Requirements on unit test coverage.
* Requirements on integration test coverage.
* Requirements on end-to-end test coverage (where applicable).
* One-off manual testing as needed (ideally this should be minimal and in the form of adhoc/exploratory testing).

The deliverables are considered complete when the tests have been added/updated and the merge request has been merged.

#### Process

At [release kickoff](../../../workflow/#kickoff) we highlight some of the changes scheduled for the next release. The majority of our test planning work starts in the issues relevant to those changes, although this process can be applied to any change. Here is an overview of the process:

* Discuss how the change could affect quality (in the feature issue and/or merge request).
* Review test coverage and list test deliverables for [tests at different levels](https://docs.gitlab.com/ee/development/testing_guide/testing_levels.html).
* Add/update tests.

The following guidelines provide more detail, as well as suggested responsibilities for various roles.

##### As a feature issue author:
{:.no_toc}

* Use the issue to discuss how the change could affect the quality of the product and impact our users.
  * Start the discussion by answering the questions in the Testing section of the [feature proposal template](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab/issue_templates/Feature%20proposal.md). Note that those questions are not exhaustive.
  * [Optional] See the [Test Plan](#test-plan) section for advice on when a test plan might be useful.
##### As a Product Manager, Product Designer, Engineer (of any type), user, or anyone else involved in the change:
{:.no_toc}

* Continue the discussion of quality and risk that was started in the issue description. Share any insights that you have that could help guide testing efforts.

##### As an Engineer who will implement the change, or a Test Automation Engineer contributing to the change:
{:.no_toc}

* Use the issue to start a discussion about test strategy, to come up with clear test deliverables for tests at different levels.
  * List the test deliverables in the feature merge request(s).
  * You can use [test design heuristics](test-design/) to determine what tests are required. It's not necessary to use test design heuristics explicitly, but it can be helpful to clarify how you come up with tests; it helps to guide discussion and create shared understanding.
* If a merge request touches the feature specs `spec/features`, involve your counterpart test automation engineer to review the merge request.
* If a feature requires an end-to-end test, add a `Requires e2e tests` label to the feature issue or merge request.
  * Before merging the feature merge request, ensure the end-to-end test merge request is linked to the feature merge request.

##### As a merge request author (i.e., the Engineer who will implement the test):
{:.no_toc}

* Complete the test deliverables.
 * End-to-end tests should be included in the feature merge request where possible, but can be in a separate merge request (e.g., if being written by a different engineer).
 * All lower-level tests **must** be included in the feature merge request.

##### As a Test Automation Engineer:
{:.no_toc}

* Help guide the discussions in issues and merge requests, and ensure that we complete the test coverage as planned before the feature is merged into `master` and released to production.

Finally, once all test deliverables are completed, the feature issue can be closed (along with a test plan, if one was created).

Everyone in engineering is expected to contribute to Quality and keep our test pyramid in top shape.
For every new feature we aim to ship a new slice of the pyramid so we don't incur test automation debt.
This is what enables us to do Continuous Delivery.

![TestPyramid.png](TestPyramid.png)

#### Test Plan

We do not require a test plan for every feature or epic. Test plans are expensive to create and we would only limit this to high-impact and cross functional changes.
There is no strict guideline for this and we defer this decision to each engineering group or team.

**Examples of work that's likely to warrant a test plan:**
* Swapping underlying infrastructure providers (e.g., the [GCP migration](https://gitlab.com/gitlab-com/migration/issues/451#test-plan)).
* Certifying performance improvements for customers.
* Upgrading underlying Rails version (e.g., the [Rails 5 migration](https://gitlab.com/gitlab-org/gitlab-ce/issues/51719)).

GitLab's test plan is based on [Google’s 10 min test plan](https://testing.googleblog.com/2011/09/10-minute-test-plan.html).
This test plan uses the ACC Framework (Attribute, Components and Capabilities matrix)
* Attributes: qualities the product should have
* Components: major parts of the product
* Capabilities: behavior the product should display that links components and attributes

Currently we have a [test plan template](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/.gitlab/issue_templates/Test%20plan.md) that is available for test planning in GitLab CE & EE. A test plan can be created by anyone involved in a change.

You can see the all the existing test plans here:
* [GitLab CE Test Plans](https://gitlab.com/gitlab-org/gitlab-ce/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=test-plan)
* [GitLab EE Test Plans](https://gitlab.com/gitlab-org/gitlab-ee/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=test-plan)

#### Security

In addition to quality, [security](../../../security/) is also one of our top concerns. With every change and every new feature we need to consider security risks and mitigate them early in the development process.

See also when and how to request a [security review](../../../security/#internal-application-security-reviews).

The test plan template has a risk column dedicated to security, however, whether or not a test plan is not created, it is essential that a discussion about security risks takes place as early as possible.

### Test Coverage Tracking

We are currently evaluating using GitLab mechanics to track test coverage in [gitlab-org/quality/testcases](https://gitlab.com/gitlab-org/quality/testcases/issues).

Our previous iteration can be seen at [DEPRECATED - GitLab Blackbox Integration & E2E Test Coverage](https://docs.google.com/spreadsheets/d/1RlLfXGboJmNVIPP9jgFV5sXIACGfdcFq1tKd7xnlb74/edit).

An explanation of why we chose to do it this way is explained in this issue: [Test case management and test tracking in a Native Continuous Delivery way](https://gitlab.com/gitlab-org/gitlab-ce/issues/51790).

To summarize, we want to track our tests in a Native Continuous Delivery way.
* If we are doing continuous delivery in the right way, there should be little to no manual tests. Manual tests if any should be minimal and exploratory.
* There should be little need for detailed test steps if manual testing is minimal and exploratory.
  * Exploratory testing has a free form emphasis which removes the importance of hardcoded test steps.
  * The disadvantage of going into too much detail with hardcoded test steps is we are not able to catch bugs outside the hardcoded flow in the document, it's also a high maintenance document.
  * Our test case name / description contains just enough description, this helps fuzzes the workflow up and we end up catching more critical bugs this way.
* An emphasis on risk-based test planning using the ACC framework.
* An emphasis on categorizing test automation types which tracks the pyramid shape (API, UI, and Visual).
* An emphasis on tracking alignment in the lower test levels in the pyramid.
* An emphasis on intelligent test design using [Test Design heuristics](test-design).

An overview of the test management view is shown below.

![TestCaseManagement.png](TestCaseManagement.png)
