---
layout: markdown_page
title: How to Be a First Response Time Hawk
category: Handling tickets
---

### On this page
{:.no_toc}

- TOC
{:toc}


### What is a First Response Time Hawk?

At GitLab, we want to create the support team of the future. Remote is a great first step, but one of the biggest challenges in any support organization is accountability. How do we maintain accountability while also encouraging flexibility? First Response Time Hawk.

This is a rotating role, that will have someone be "on point" weekly, to make sure our Premium SLA tickets get a response and are appropriately categorized and prioritized.

### The Process in Action

1. Every Week on Fridays, Managers will add a First Response Hawk for each region to the Support Week in Review to announce who will be responsible that week.
1. If you are FRT Hawk, you are responsible for:
    1. Premium and Ultimate first replies for tickets created from 9am - 5pm in your time zone
    1. Checking and filing tickets in the 'Needs Org and Triage' view 
    1. Setting Priority on Tickets as per our [Definitions of Support Impact](https://about.gitlab.com/support/#definitions-of-support-impact)
    1. Ensuring that tickets have the correct **Self-managed Problem Type** selected
1. Sort the Zendesk view by 'Requested' descending so you can keep an eye on newly requested tickets (click the word 'Requested' to change the sort order, when done you can click 'Reset Sort Order' to return to the default view sorting.
1. You have full authority to call in and ask others for help if volume is high, or you are stumped.
1. You should expect to see new and different things that you are not an expert in. Take the time to dive deep and try and understand the problem.

### Things to be mindful of

1. Given that there's a FRT Hawk _per timezone_, your shift will overlap with at least another Hawk. 
    * This means you have a teammate within a team; yay! Be sure to communicate with your fellow hawk so you can help each other out in case one of you is stuck on a ticket, and also to make sure you're not 
    both quietly working on the same ticket
    
1. While your focus should be new tickets in the Premium & Ultimate queue, do not forgot about the tickets in the Starter & Free queue
   * This is especially true if we're having a busy week. Find a balance between new tickets in each queue
   
1. You might not cover all the tickets you wanted to cover on your shift, and that's okay
   * Just do your best, 
   * Take a break if you need one(make sure your fellow hawk knows you're stepping away
   * Ask the rest of the team for help when needed.

### Dashboards

You can track your progress and see global results [on this Zendesk Dashboard](https://gitlab.zendesk.com/agent/reporting/analytics/period:0/dashboard:ab7DLgKtesWr)

