---
layout: markdown_page
title: "Tech Stack Applications"
---

## GitLab Systems Diagram
![](/images/handbook/business-ops/Systems_Diagram.png)

## Tech Stack Applications

| [Applications & Tools](https://about.gitlab.com/handbook/business-ops/tech-stack/) | Business Purpose | Who Should Have Access | Contact/Admin(s) | [Data Classification](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html) | [Okta](https://about.gitlab.com/handbook/business-ops/okta/) | In-scope  for  SOX? |
| --- | --- | --- | --- | --- | --- | --- |
| [1Password](https://about.gitlab.com/handbook/business-ops/tech-stack/#1password) |   | Team Members |   | Red | Yes |   |
| [ADP](https://about.gitlab.com/handbook/business-ops/tech-stack/#adp) |   | Finance Team | **Ann Tiseo (Contact)**<br>Jenny Nguyen (Contact)<br>Wilson Lau (Admin) | Red | Yes |   |
| [Avalara](https://about.gitlab.com/handbook/business-ops/tech-stack/#avalara) |   | Finance Team | Wilson Lau (Contact/Admin) |  | Yes |   |
| [Balsamiq Cloud](https://about.gitlab.com/handbook/business-ops/tech-stack/#balsamiq-cloud) |   | Product Designers<br>UX Researchers<br>Product Managers | **Christie Lenneville (Primary/Admin)**<br>Sarah O'Donnell (Secondary/Admin) |  | Yes |   |
| [BambooHR](https://about.gitlab.com/handbook/business-ops/tech-stack/#bamboohr) |   | Team Members | PeopleOPS | Red | Yes |   |
| [Bizible](https://about.gitlab.com/handbook/business-ops/tech-stack/#bizible) |   | Marketing OPS<br>Online Growth<br>Marketing Programs | JJ Cordz (Contact/Admin) |  | No |   |
| [Blackline](https://about.gitlab.com/handbook/business-ops/tech-stack/#blackline) |   |   |   |   | No |   |
| [By Appointment Only (BAO)](https://about.gitlab.com/handbook/business-ops/tech-stack/#by-appointment-only-bao) |   | Online Marketing<br>SDR Team Lead | **Jeffrey Broussard (Primary/Admin)**<br>JJ Cordz |  | No |   |
| [Bonusly](https://about.gitlab.com/handbook/business-ops/tech-stack/#bonusly) |   |   |   |   | Yes |   |
| [Calendly](https://about.gitlab.com/handbook/business-ops/tech-stack/#calendly) |   | Team Member |   | Yellow | Yes |   |
| [Carta](https://about.gitlab.com/handbook/business-ops/tech-stack/#carta) |   | Access via GitLab credentials | Paul Machle (Contact/Admin) |  | Yes |   |
| [Chorus](https://about.gitlab.com/handbook/business-ops/tech-stack/#chorus) |   | **Recorders:**<br>US East Sales<br>US West Sales<br>Solutions Architects<br>US SDR Team<br>US BDR Team<br><br>**Listeners:**<br>Everyone Else | **Alex Tkach (Primary)**<br>Francis Aquino<br>David Somers |   | No |   |
| [Clari](https://about.gitlab.com/handbook/business-ops/tech-stack/#clari) |   | Quota Carrying Sales Reps | Alex Tkach (Contact/Admin) |  | Yes |   |
| [CloudExtend: Google Drive for Netsuite](https://about.gitlab.com/handbook/business-ops/tech-stack/#cloudextend-google-drive-for-netsuite) |   | Finance Team | Cindy Nunez (Contact/Admin) |   |   |   |
| [Conga Contracts](https://about.gitlab.com/handbook/business-ops/tech-stack/#conga-contracts) |   | Legal Team | **Legal Team** |   |   |   |
| [ContractWorks](https://about.gitlab.com/handbook/business-ops/tech-stack/#contractworks) |   | Legal Team | **Legal Team** |   | Yes |   |
| [Cookiebot](https://about.gitlab.com/handbook/business-ops/tech-stack/#cookiebot) |   | Marketing OPS | JJ Cordz (Contact/Admin) |   | No |   |
| [Crowdin.com](https://about.gitlab.com/handbook/business-ops/tech-stack/#crowdincom) |   |   | PeopleOPS |   | Yes |   |
| [CultureAmp](https://about.gitlab.com/handbook/business-ops/tech-stack/#cultureamp) |   | Team Members | PeopleOPS | Yellow | Yes |   |
| [Customers.GitLab](https://about.gitlab.com/handbook/business-ops/tech-stack/#customersgitlabcom) |   |   |   | Red | No |   |
| [DataFox](https://about.gitlab.com/handbook/business-ops/tech-stack/#datafox) |   | Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound SDR | **Alex Tkach (Primary/Admin)**<br>Francis Aquino(Admin) |  | No |   |
| [GitLab Dev](https://about.gitlab.com/handbook/business-ops/tech-stack/#gitlab-dev) |   |   |   |   | No |   |
| [Digital Ocean](https://about.gitlab.com/handbook/business-ops/tech-stack/#digitalocean) |   |   |   |   | Yes |   |
| [DiscoverOrg](https://about.gitlab.com/handbook/business-ops/tech-stack/#discoverorg) |   | Mid-Market AEs<br>Outbound SDR | Alex Tkach (Contact/Admin)<br>JJ Cordz |   | Yes |   |
| [Disqus](https://about.gitlab.com/handbook/business-ops/tech-stack/#disqus) |   | Varies | JJ Cordz (Contact/Admin) |   | Yes |   |
| [DoGood](https://about.gitlab.com/handbook/business-ops/tech-stack/#dogood) |   | Online Marketing<br>SDR Team Lead | **JJ Cordz (Admin)**<br>Jeffrey Broussard (Contact) |   | No |   |
| [Drift](https://about.gitlab.com/handbook/business-ops/tech-stack/#drift) |   | Inbound BDR | JJ Cordz (Contact/Admin) |   | Yes |   |
| [Elastic Cloud](https://about.gitlab.com/handbook/business-ops/tech-stack/#elastic-cloud) |   |   |   |   | No |   |
| [Eventbrite](https://about.gitlab.com/handbook/business-ops/tech-stack/#eventbrite) |   | Field Marketing<br>Content Marketing<br>Marketing Programs<br>Marketing OPS | JJ Cordz (Contact/Admin) |   | Yes |   |
| [Expensify](https://about.gitlab.com/handbook/business-ops/tech-stack/#expensify) |   | Access via GitLab credentials | Wilson Lau (Contact/Admin) | Orange | Yes |   |
| [Fastly](https://about.gitlab.com/handbook/business-ops/tech-stack/#fastly) |   |   |   |   | Yes |   |
| [FunnelCake](https://about.gitlab.com/handbook/business-ops/tech-stack/#funnelcake) |   | Marketing OPS<br>Online Marketing | JJ Cordz (Contact/Admin) |   | No |   |
| [G-Suite](https://about.gitlab.com/handbook/business-ops/tech-stack/#g-suite) |   | Team Members | IT OPS | Red | Yes |   |
| [GitLab](https://about.gitlab.com/handbook/business-ops/tech-stack/#gitlab) |   | GitLab Application for Team Members |   | Red | No |   |
| [Google Adwords](https://about.gitlab.com/handbook/business-ops/tech-stack/#google-adwords) |   | Marketing OPS<br>Online Marketing<br>Product Marketing<br>Content Marketing<br>Marketing Programs | JJ Cordz (Contact/Admin) |   |   |   |
| [Google Analytics](https://about.gitlab.com/handbook/business-ops/tech-stack/#google-analytics) |   | Marketing OPS<br>Online Marketing<br>Product Marketing | **LJ Banks (Primary)**<br>JJ Cordz (Admin) |   |   |   |
| [Google Cloud Platform](https://about.gitlab.com/handbook/business-ops/tech-stack/#google-cloud-platform) |   |   |   |   | Yes |   |
| [Google Search Console](https://about.gitlab.com/handbook/business-ops/tech-stack/#google-search-console) |   | Marketing OPS<br>Online Marketing<br>Production | **JJ Cordz (Primary/Admin)**<br>Andrew Newdigate (Admin) |   |   |   |
| [Google Tag Manager](https://about.gitlab.com/handbook/business-ops/tech-stack/#google-tag-manager) |   | Online Marketing | JJ Cordz (Contact/Admin) |   |   |   |
| [Greenhouse](https://about.gitlab.com/handbook/business-ops/tech-stack/#greenhouse) |   | People Operations | Erich Wegscheider (Contact/Admin) | Red | Yes |   |
| [GovWin IQ](https://about.gitlab.com/handbook/business-ops/tech-stack/#govwin-iq) |   | Sales-Federal | Alex Tkach (Contact/Admin) |   | Yes |   |
| [HackerOne](https://about.gitlab.com/handbook/business-ops/tech-stack/#hackerone) |   |   |   |   | Yes |   |
| [Lean Data](https://about.gitlab.com/handbook/business-ops/tech-stack/#leandata) |   | Marketing OPS<br>Sales OPS | **JJ Cordz (Primary/Admin)**<br>Alex Tkach |   | No |   |
| [LicenseApp](https://about.gitlab.com/handbook/business-ops/tech-stack/#leandata) |   | Access via GitLab credentials | Alex Tkach (Contact/Admin) |   |   |   |
| [LinkedIn Sales Navigator](https://about.gitlab.com/handbook/business-ops/tech-stack/#linkedin-sales-navigator) |   | Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR | JJ Cordz (Contact/Admin) |   | Yes |   |
| [MailChimp](https://about.gitlab.com/handbook/business-ops/tech-stack/#mailchimp) |   | Marketing OPS<br>UX Team<br>Product Marketing<br>Marketing Programs | JJ Cordz (Contact/Admin) |   |   |   |
| [MailGun](https://about.gitlab.com/handbook/business-ops/tech-stack/#mailgun) |   |   | John Northup (Admin) |   | Yes |   |
| [Marketo](https://about.gitlab.com/handbook/business-ops/tech-stack/#marketo) |   | Marketing OPS | JJ Cordz (Contact/Admin) |   | Yes |   |
| [Moo](https://about.gitlab.com/handbook/business-ops/tech-stack/#moo) |   | Team Members |   | Yellow | Yes |   |
| [Moz Pro](https://about.gitlab.com/handbook/business-ops/tech-stack/#moz-pro) |   | Online Growth | JJ Cordz (Contact/Admin) |   |   |   |
| [Netsuite](https://about.gitlab.com/handbook/business-ops/tech-stack/#netsuite) |   | Finance Team | **Cindy Nunez (Primary/Admin)**<br>Wilson Lau (Admin) |   | Yes |   |
| [NexTravel](https://about.gitlab.com/handbook/business-ops/tech-stack/#nextravel) |   | GitLab Team Members | Cassiana Gudgenov (Primary)<br>Trevor Knudson (Contact)<br>Nadia Vatalidis (Contact)<br>Kirsten Abma (Admin) | Orange | Yes |   |
| [Ops - GitLab](https://about.gitlab.com/handbook/business-ops/tech-stack/#ops---gitlab) |   |   |   |   |   |   |
| [Okta](https://about.gitlab.com/handbook/business-ops/tech-stack/#okta) |   | GitLab Team Members | IT Ops (Contact)<br>Amber Lammers (Admin) |   |   |   |
| [Outreach.io](https://about.gitlab.com/handbook/business-ops/tech-stack/#outreachio) |   | Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR | **JJ Cordz (Primary/Admin)**<br>Alex Tkach (Contact/Admin)<br>Michael Snow (Admin) |   | Yes |   |
| [OwnBackup](https://about.gitlab.com/handbook/business-ops/tech-stack/#ownbackup) |   | Business Operations | **Michael Snow (Primary/Admin)**<br>Jack Brennan (Secondary) |   | Yes |   |
| [PackageCloud](https://about.gitlab.com/handbook/business-ops/tech-stack/#packagecloud) |   |   |   |   | Yes |   |
| [PagerDuty](https://about.gitlab.com/handbook/business-ops/tech-stack/#pagerduty) |   |   |   |   | Yes |   |
| [Periscope](https://about.gitlab.com/handbook/business-ops/tech-stack/#periscope) |   | All GitLabbers | **Taylor Murphy (Primary/Admin)**<br>Emilie Schario | Red |   |   |
| [Rollup Helper](https://about.gitlab.com/handbook/business-ops/tech-stack/#rollup-helper) |   |   |   |   |   |   |
| [Salesforce](https://about.gitlab.com/handbook/business-ops/tech-stack/#salesforce) |   | Sales Leadership<br>Account Executives<br>Account Managers<br>Outbound SDR<br>Inbound BDR<br>Solutions Architects<br>Marketing OPS<br>Product Marketing<br>Marketing Programs | Not related to quotes or lead routing: **Alex Tkach (Primary/Admin)**<br>Francis Aquino (Admin)<br><br>Lead Routing/Scoring/Source: **JJ Cordz (Admin)**<br><br>SFDC Automation/Code/Config: **Jack Brennan (Admin)** |   | Yes |   |
| [Screaming Frog](https://about.gitlab.com/handbook/business-ops/tech-stack/#screaming-frog) |   | Online Growth | **Shane Rice (Primary/Admin)**<br>LJ Banks (Secondary) |   |   |   |
| [Sertifi](https://about.gitlab.com/handbook/business-ops/tech-stack/#sertifi) |   | Account Executives<br>Account Managers | Alex Tkach (Contact/Admin) | Yellow | Yes |   |
| [Sketch](https://about.gitlab.com/handbook/business-ops/tech-stack/#sketch) |   | Product Designers | **Christie Lenneville (Primary/Admin)**<br>Taurie Davis (Contact/Admin)<br>Sarah O’Donnell (Contact/Admin) |   | No |   |
| [Slack](https://about.gitlab.com/handbook/business-ops/tech-stack/#slack) |   | Team Members | **Refer to Tech Stack details for full list of Contacts/Admins | Red | Yes |   |
| [Snowflake (Data Warehouse)](https://about.gitlab.com/handbook/business-ops/tech-stack/#snowflake) |   | Data Team | **Taylor Murphy (Contact/Admin)**<br>Thomas La Piana (Contact) |   | Yes |   |
| [Sprout Social](https://about.gitlab.com/handbook/business-ops/tech-stack/#sprout-social) |   | Content Marketing<br>Marketing OPS | **JJ Cordz (Primary/Admin)**<br>Emily von Hoffman (Secondary) |   | Yes |   |
| [Staging-GitLab](https://about.gitlab.com/handbook/business-ops/tech-stack/#staging-gitlab) |   |   |   |   | No |   |
| [Stripe](https://about.gitlab.com/handbook/business-ops/tech-stack/#stripe) |   | Finance Team | Wilson Lau (Contact/Admin) |   | Yes|   |
| [Survey Monkey (Marketing)](https://about.gitlab.com/handbook/business-ops/tech-stack/#survey-monkey) |   | Marketing | JJ Cordz (Contact/Admin) |   |   |   |
| [Survey Monkey (UX)](https://about.gitlab.com/handbook/business-ops/tech-stack/#survey-monkey) |   | UX Researchers | Sarah O’Donnell (Contact/Admin) |   |   |   |
| [Status - IO](https://about.gitlab.com/handbook/business-ops/tech-stack/#status-io) |   |   |   |   | Yes |   |
| [Tweetdeck](https://about.gitlab.com/handbook/business-ops/tech-stack/#tweetdeck) |   | Varies | JJ Cordz (Contact/Admin) |   | Yes |   |
| [UsabilityHub](https://about.gitlab.com/handbook/business-ops/tech-stack/#usabilityhub) |   | UX Researchers<br>Product Designers | Sarah O’Donnell (Contact/Admin) |   |   |   |
| [Visual Compliance](https://about.gitlab.com/handbook/business-ops/tech-stack/#visual-compliance) |   | Access via Salesforce | **Jamie Hurewitz (Primary)**<br>Related to Technical implementation: Michael Snow (Secondary/Admin) |   |   |   |
| [WebEx](https://about.gitlab.com/handbook/business-ops/tech-stack/#webex) |   | Marketing OPS<br>Public Sector | JJ Cordz (Contact/Admin) |   |   |   |
| [Will Learning](https://about.gitlab.com/handbook/business-ops/tech-stack/#will-learning) |   |   |   | Yellow | No |   |
| [Xactly](https://about.gitlab.com/handbook/business-ops/tech-stack/#xactly) |   | Sales Leadership<br>Finance Team | **Matt Benzaquen (Primary/Admin)**<br>Wilson Lau (Secondary/Admin) |   | Yes |   |
| [YouTube](https://about.gitlab.com/handbook/business-ops/tech-stack/#youtube) |   | Varies | JJ Cordz (Contact/Admin) |   | Yes |   |
| [Zapier](https://about.gitlab.com/handbook/business-ops/tech-stack/#zapier) |   |   | IT Ops |   | Yes |   |
| [Zendesk](https://about.gitlab.com/handbook/business-ops/tech-stack/#zendesk) |   | Access via Salesforce | **Lee Matos (Primary Contact)**<br>Alex Tkach (Admin) |   | Yes |   |
| [Zoom](https://about.gitlab.com/handbook/business-ops/tech-stack/#zoom) |   | Access via GitLab credentials | **PeopleOPS** | Red | Yes |   |
| [Zuora](https://about.gitlab.com/handbook/business-ops/tech-stack/#zuora) |   | Access via Salesforce | **Wilson Lau (Primary)**<br>Francis Aquino (Secondary/Admin) |   | Yes |   |
