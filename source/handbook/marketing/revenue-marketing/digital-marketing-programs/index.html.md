---
layout: markdown_page
title: "Digital Marketing Programs"
---

## On this page

{:.no_toc}

- TOC
{:toc}

## Digital Marketing Programs (DMP)

The Digital Marketing Programs team attracts new visitors through organic and paid channels, tests incremental changes for conversion rate improvement, builds and manages marketing campaigns, and tracks campaign performance.  There are two primary roles on this team:
### Marketing Program Managers 
* [Role](https://about.gitlab.com/job-families/marketing/marketing-program-manager/)
* [Responsibilities](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)

### Digital Marketing Managers 
* [Role](https://about.gitlab.com/job-families/marketing/digital-marketing-programs-manager/)
* [Responsibilities](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/)

## DMP responsibilities
* Search Engine Optimization (SEO)
* Web analytics and web metrics dashboards
* Website optimization
* Marketing program management for email campaigns, event promotion, event follow up, drip email nurture series, webinars, and content
* Paid media management
* Email marketing
* A/B testing and Conversion Rate Optimization (CRO)

## Integrated campaigns
[Integrated campaigns](/handbook/marketing/#integrated-campaigns) are managed by the Marketing Programs team.

## Reporting

Information about reporting done by the Digital Marketing Programs team and across the Marketing functional group can be found in the [Business Operations - Reporting](/handbook/business-ops/#reporting) section.

## Digital Marketing Tools

We use a variety of tools to support the other team within the company. Details about the tech stack, who has access and the system admins is found on the [Tech Stack Applications](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) page of the Business Operations handbook section.

