---
layout: markdown_page
title: "Evangelist Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

At GitLab our mission is to change all creative work from read-only to read-write so that **everyone can contribute**. In order to fulfill this mission, we need to create both the tools and platform to enable this change and a community of contributors who share our mission. We are just getting started in building the GitLab community and we encourage everyone to contribute to its growth.

There are many ways to participate in the GitLab community today: [contributing to an open source project](https://about.gitlab.com/handbook/marketing/community-relations/code-contributor-program/), [contributing to our documentation](https://docs.gitlab.com/ee/development/documentation/), [hosting your open source project on GitLab](https://about.gitlab.com/solutions/open-source/), or teaching your colleagues and collaborators about the value of [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/).

We are building an evangelist program to support people who share our mission and want to give tech talks, run local meetups, or create videos or blogs. We will be announcing more in Q1. For now, please email `evangelists@gitlab.com` if you have feedback on our vision, ideas for how we can build our community, or suggestions for a name for our evangelist program.

## How to see what we're working on

We use the `Evangelist Program` label to track issues. The [Evangelist Program issue board](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/boards/951386?&label_name[]=Evangelist%20Program) provides an overview of these issues and their status.

For meetups, the `Meetups` label is used to track meetup issues within the marketing group. These issues typically exist in the `Evangelist Program` subgroup but they can also exist in Field Marketing, Corporate Marketing, or other marketing subgroups. They are listed in the [Q2 OKR Epic](https://gitlab.com/groups/gitlab-com/marketing/community-relations/evangelist-program/-/epics/4) and on the [Meetups board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?&label_name[]=Meetups). 

We plan to replicate this process of having a label, epic, and issue board for future OKRs and will update this page so you can see what we are working on and track our progress.

### Heroes

We are also working to build the GitLab Heroes program which will help us to better engage, support, and thank members of the wider GitLab community who serve as advocates of GitLab in tech communities around the globe. You can track our progress, comment, and contribute via the [GitLab Heroes Epic](https://gitlab.com/groups/gitlab-com/marketing/community-relations/evangelist-program/-/epics/3) and related issues.

### Upcoming events

The [Community Events calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) includes a list of our upcoming meetups, hackathons, office hours, and other community events.

### GitLab presentations given at meetups [KPI](/handbook/ceo/kpis/) Definition

A GitLab-related presentation is defined as a presentation given by a GitLab-employee or a member of the GitLab community about GitLab or a tangential topic.
It does not include meetups where GitLab is only a sponsor and does not have a speaking slot.
The goal is 10 per fiscal quarter.
This is currently tracked by counting the closed issues on the [Meetup Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name[]=Meetups), which are issues with the "Meetup" label.
This method of tracking is imperfect for many reasons.
Some issues are created in other projects.
There are GitLab-related presentations delivered at meetups that we do not discover until after the fact, if at all.


### Participants at Meetups with GitLab presentation [KPI](/handbook/ceo/kpis/) Definition

This KPI tracks the number of attendees at meetups where a GitLab-related presentation is delivered, as defined above.
Because of the difficulties mentioned above, it can be hard to identify all meetups with GitLab presentations.
A proxy metric here can be to estimate attendance from RSVPs on Meetup.com.
The Evangelist Program Manager estimates that mettup attendance ranges between 35 and 50% of RSVPs.

## Meetups

GitLab supports team members and members of the wider GitLab community who want to organize or speak at meetups. Our goal in supporting these events to better engage with and increase connections among the GitLab community, increase awareness of GitLab, and better educate the technology community.

### Organize a meetup

- We love and support meetups. If you participate in local tech groups and are interested in having a GitLab speaker or GitLab as a sponsor, please submit an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request). Please note, providing sufficient lead time (at least a month) allows us to better support your event so feel free to reach out as soon as possible.
- If you are interested in creating your own GitLab meetup or if you already have an existing meetup on meetup.com that you would like to link to GitLab's meetup.com Pro account, please email `evangelists@gitlab.com`.  You can find the list of GitLab meetups on the [meetup.com page](https://www.meetup.com/pro/gitlab).
- When you are getting started, we recommend scheduling at least 2 meetups. Publish your first meeting with a date and topic, and then a second meeting with a date and topic. The second meeting can have a flexible topic based on how the first meeting goes. The point of setting two meet-ups is to help build momentum in the new group.
- Often, we try to have GitLab employees attend the first couple of meetups. Once the group has hosted a couple of successful events, it becomes easier for the group to sustain itself. It is much harder to start new meetups versus maintaining existing ones. So we make an effort to support and keep new meetups going.
- Reach out to other like-topic meetups and invite them to your meetup to help grow your community.
- Once you have scheduled your meetup, add the event to our [events page](/events/) so the whole world knows! Check out the [How to add an event to the events page](/handbook/marketing/corporate-marketing/#how-to-add-events-to-the-aboutgitlabcomevents-page) section if you need help on how to add an event.
- If you purchase any food & beverage for the meetup event, we can help cover the costs.  A general guideline is $US 5/person for a maximum of $US 500 per each meetup.
- To be reimbursed for your expenses, please send an invoice and receipts for the event to [evangelists@gitlab.com](mailto:evangelists@gitlab.com). We prefer organizers use our [Meetup Invoice template](https://docs.google.com/spreadsheets/d/1D3lpPrwfz1zQp8LsiWUq64aBNYHnlKbhjyPf6sQ8NsM/edit?usp=sharing).
- We recommend all Meetup organizers record their events, when possible. Options include:
  - Using Quicktime to simultaneously conduct screen recording and audio recording. The two recordings can be synced into a single video file.
  - To record the speaker, we recommend using a webcam with a microphone (such as the [Logitech C922x](https://www.amazon.com/Logitech-C922x-Pro-Stream-Webcam/dp/B01LXCDPPK/ref=sr_1_2?crid=13ZBFCYG30ULV&keywords=logitech+webcam&qid=1552336114&s=gateway&sprefix=logitech+%2Caps%2C142&sr=8-2)). You can [insert the slides as images](https://support.apple.com/en-us/HT204674) into the video recording later using iMovie or your preferred video editing software.

### Speak at a meetup

Meetups help us raise awarness of GitLab and build communities in new places. We love to track them to know where the community is growing. If you are speaking at a meetup as a representative of GitLab or you are giving a talk about GitLab, please let us know! Here's how and why we do this:

- Speakers should use the [Meetup Speaker template](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-speaker) to create a new issue with the details for your meetup talk.
- Opening an issue helps us support you. A few ways we can help:
  - Marketing: We'll share the event on our blog, via social, and with GitLab community members in the area to raise awareness of the event.
  - Sponsorship: GitLab will cover the cost of food and beverages for meetup organizers.
  - Swag: Our swag is pretty popular so we'll send you plenty of stickers and *maybe* some other fun stuff to give away.
  - Speaker prep: If you need help with your deck or would like someone to offer feedback on a dry run of your talk, the [Evangelist Program Manager](mailto:evangelists@gitlab.com) is happy to help.
- We track meetups via issues on the [Meetups issue board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/962542?label_name[]=Meetups). Take a look and see what we have coming up!

### Other meetups

We use Zapier to scan Meetup.com and Twitter for GitLab events that occur outside the scope of our program. Events containing "GitLab" on Meetup.com or tweets containing "GitLab Meetup" are added to the [GitLab Meetups](https://docs.google.com/spreadsheets/d/1pgXbLnquacqgMkT3T1-Q3EN5cr1Fq0SgCMwda43-Hdk/edit?usp=sharing) sheet and shared in Slack in the [#gitlab-meetups-feed](https://gitlab.slack.com/messages/CHHC8PFNF) channel. We review these events each week and reach out to thank organizers and speakers who are raising awareness for GitLab. In many cases, these organizers and speakers will also be offered support with future GitLab-related events.

## Community events

We'd love to support you if you are organizing or speaking at a community-driven event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.). Depending on the number and type of attendees at an event, it may be owned by [Corporate Marketing](https://about.gitlab.com/handbook/marketing/#new-ideas-for-marketing-campaigns-or-events), [Field Marketing](https://about.gitlab.com/handbook/marketing/marketing-sales-development/field-marketing/#evaluating-potential-field-initiatives), or Community Relations. Our events decision tree is a guide to help you find the right team to handle an event request.

Events Decision Tree:
![event decision tree](/images/handbook/marketing/event-decision-tree.png)

### Submit an event request to our team

To submit a community event for support or sponsorship:

1. Review our events decision tree to ensure you are directing your event inquiry to the appropriate team.
1. Submit an issue using the [sponsorship-request](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=sponsorship-request) template.
1. For Service Desk or other auto-generated issues that contain sponsorship requests, we will retroactively apply the 'sponsorship-request' to the issue. The process for updating an issue with no template to the 'sponsorship-request' template is: copy text from original issue, assign 'sponsorship-request' template to issue, paste text from original issue into the appropriate field at bottom of template, update remaining fields.
1. GitLab XDRs: for *contact requests* received in Salesforce via the [Sales webform](https://about.gitlab.com/sales/) requesting event sponsorship, please change ownership to `GitLab Evangelist` in SFDC & be sure to "check the box" to send a notification.
1. GitLab's Evangelist Program Manager will review the request and follow up with the event contact.

### How we assess requests

We ask the following questions when assessing an event:
- Will there be a GitLab speaker? We do not require a speaker slot in return for sponsorship but we do prioritize events where the audience will be hearing about GitLab - either from a GitLab team member or a member of the wider GitLab community.
- What type of people will be attending the event? We prioritize events attended by diverse groups of developers with an interest in DevSecOps, Cloud Native, Kubernetes, Serverless, Multi-cloud, CI/CD, Open Source, and other related topics.
- Will we be able to interact with attendees? We prioritize events that provide opportunities for meetings, workshops, booth or stands to help people find us, and other interaction with attendees.
- Where will the event be held? We aim to have a presence at events around the globe with a particular focus on areas with large GitLab communities and large populations of developers.
- Is the event important for industry and/or open source visibility? We prioritize events that influence trends and attract leaders within the developer and open source communities. We also prioritize events organized by our strategic partners.
- What is the size of the opportunity for the event? We prioritize events based on audience size, the number of interactions we have with attendees, and potential for future contributions to GitLab from attendees.

Each question is graded on a scale of 0-2. We then tally the scores and assign the event to a sponsorship tier.

- Events scoring below 5 are not eligible for sponsorship.
- Events scoring 6-8 are eligible for GitLab swag for attendees and other in-kind sponsorship.
- Events scoring 9 or above are eligible for financial support.

We ask these questions and use this scorecard to ensure that we're prioritizing GitLab's and our community's best interests when we sponsor events.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

## Find a tech speaker

We'd love to support you if you are organizing an event, be it GitLab-centric or around a topic where GitLab content is relevant (e.g. DevOps meetup, hackathon, etc.).

You can get in touch with speakers from the GitLab team and the wider community to participate and do a talk at your event. We maintain a list of active speakers on our [Find a GitLab speaker](https://about.gitlab.com/events/find-a-speaker/) page. Once you find a speaker in your region, contact them directly. For GitLab team members, you can also check the #cfp channel on Slack where many of our active tech speakers will see your speaker request. Most speakers will also be able to do talks remotely if the event is virtual or if travel is a challenge.

If you have questions, you can always reach us by sending an e-mail to `evangelists@gitlab.com`.

## Become a tech speaker

If you are aware of people from the GitLab community who are interested in giving a tech talk relating to GitLab, please direct them to our [Become a Speaker](https://about.gitlab.com/community/evangelists/become-a-speaker/) page for more information on the type of support we provide.

For GitLab team members who want to become a tech speaker, contact `evangelists@gitlab.com` and check out the #cfp channel on Slack to discover upcoming opportunities. Additional detail on the logistics of giving a talk once your proposal has been accepted can be found on the [Corporate Marketing](https://about.gitlab.com/handbook/marketing/corporate-marketing/#speakers) page.

### Resources for speakers

- [Presentation template](https://about.gitlab.com/handbook/tools-and-tips/#google-slides-templates) - our current presentation template.
- [Customer facing presentations](https://about.gitlab.com/handbook/marketing/product-marketing/#customer-facing-presentations) - the latest ready-to-present decks from [Product Marketing](https://about.gitlab.com/handbook/marketing/product-marketing/).
- Additional GitLab-created presentations can be found within the folders contained in [Decks](https://drive.google.com/open?id=0Bz6KrzE1R_3hWGtTRElUWnBzeU0) and [Product Marketing](https://drive.google.com/open?id=0Bz6KrzE1R_3hNjJMNUt2LUJGREU) on the Marketing Drive.
- You can find available speaking opportunities by checking the [#cfp](https://gitlab.slack.com/messages/C106ACT6C) channel on Slack and the issue list for the [Speaker Needed](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues?label_name%5B%5D=Speaker+Needed) label.

## Contribute content

GitLab actively supports content contributors. Our community team tracks GitLab content and our evangelist program manager and editorial team regularly reviews the content.  If you would like to submit your content for review, please create an [issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=community-content) on our evangelist program project.

We make an effort to amplify and support content contributions that generate value for our community. Criteria we consider include: how well a post addresses an issue in the Community Writers issue tracker, how well a post aligns with our strategy and values, and how well a post is written.

As we identify posts that meet our criteria, we decide how we want to support the posts. We may also identify creators who we want to partner with on content. Writers who are looking for inspiration may want to visit our [Community Writers issue tracker](https://gitlab.com/gitlab-com/community-writers/issues) which tracks blog post ideas submitted by our community.

You can add content you find and track the status of submissions on the Quarterly Community Content issues ([Q2 Issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/128)). Submissions go through the following steps:

1. Review: review submissions for accuracy.
1. Say thanks: contact the creators to say thank you, share a swag code, and highlight other ways they can contribute. $25/creator is the standard amount.
1. Share: Post on social (with a credit to the author) or retweet.
1. Syndicate: for posts that we love or that answer a common or important question, we may ask the author if we can add the post to our Medium publication or the GitLab blog.
1. Curate: creators who have shown a depth of experience around topics important to GitLab and the wider GitLab community may be asked to submit talks or posts about said topics.

### Blog

Coming soon. Contact `evangelists@gitlab.com` if you have any questions.

### Videos

Coming soon. Contact `evangelists@gitlab.com` if you have any questions.

## Evangelist Program Office Hours

Our Evangelist Program Manager hosts office hours via Zoom every Friday at 10:30am ET excluding holidays. They want to answer your meetup, events, and public speaking questions and hear your feedback on our programs! You can see the meeting information and join the call via the [Community Events](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York) calendar.

## Helpful Resources

- [Community Events Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_90t5ue1q8kbjoq5b0r91nu7rvc%40group.calendar.google.com&ctz=America%2FNew_York)
- [Meetups Checklist](https://about.gitlab.com/community/meetups/checklist/)
- [Merchandise](/handbook/marketing/community-relations/evangelist-program/workflows/merchandise.html)
- [Find a speaker](/handbook/marketing/community-relations/evangelist-program/workflows/find-a-speaker.html)
