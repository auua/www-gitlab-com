---
layout: markdown_page
title: "Code Contributor Program"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----
## Overview

From the beginning, GitLab has been an open source project and we want to continue growing community code contribution. This will be accomplished by lowering the barrier to entry for new contributors and also by helping casual contributors become regular contributors. We want GitLab to be the open source project of choice for open source developers.  

## Communication/collaboration with contributors

### Contributor channel

A GitLab community room is available on [Gitter](https://gitter.im/gitlabhq/contributors) for people interested in contributing to GitLab. This is open to everyone to join.

### Hackathons

There will be a quarterly [Hackathon](https://about.gitlab.com/community/hackathon/) for GitLab community members to come together to work on merge requests, participate in tutorial sessions, and support each other on the [GitLab contributors channel](https://gitter.im/gitlabhq/contributors).  Agenda, logistics, materials, recordings, and other information for Hackathons will be available on the [Hackathon project pages](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/gitlab-hackathon).

The event planning will be done following the [Hackathon checklist](/handbook/marketing/community-relations/code-contributor-program/templates/hackathon-checklist.html)

### Contribute for prize

To encourage contribution to priority issues on an on-going basis (and not just during Hackathons), we will maintain a list of up to 5 priority issues for each [product stage](https://about.gitlab.com/handbook/product/categories) and prizes will be given to wider community members who have MRs merged for these issues. These issues will have the label `Contribute for prize` and more details such as prizes, assignment of these issues, etc. can be found [here](https://about.gitlab.com/community/hackathon/#contribute-for-prize).

## Supporting the contributor community

### Triaging community MR's
The `GitLab Bot` labels merge requests from outside of GitLab with the label `Community contribution` once a day (Monday-Friday). If you subscribe to the label, you will also receive an email notification from the `GitLab Bot`. It's helpful to review these MRs to do some quick triaging to apply appropriate labels and mention GitLab team-members (e.g. product managers, engineering managers, technical writers for documentation, etc. ) so that the community MRs can be reviewed in a timely manner. You can refer to the [product team handbook](https://about.gitlab.com/handbook/product/categories/#dev-department) for the list of people you can mention in  merge requests for each product area. 

### Merge Request Coaches
[Merge Request Coaches](https://about.gitlab.com/job-families/expert/merge-request-coach/) are available to help contributors with their MRs. This includes identifying reviewers for the MR, answering questions from contributors, educating contributors on the [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria), or even completing the MR if the contributor is unresponsive or unable to complete it by adding the `coach will finish` label and creating a follow-up MR. Contributors can mention the coaches in their MRs by typing `@gitlab-org/coaches`.

Merge Request Coaches can be found on the [team page](https://about.gitlab.com/company/team/) by selecting `Merge Request coach` in the department filter.  There is also the `#mr-coaching` channel in GitLab Slack if GitLab team members have any questions related to community contributions.

More information on Merge Request Coaches (including how to become a Merge Request Coach) can be found in the [MR coach lifecycle page](/handbook/marketing/community-relations/code-contributor-program/resources/merge-request-coach-lifecycle.html). 

### Reaching out to new contributors
After the first merged MR, reach out to new contributors using the [outreach email template](/handbook/marketing/community-relations/code-contributor-program/templates/email-templates.html) to congratulate them on the first MR and offer a GitLab merchandise. In cases where it would be beneficial for new contributors to be paired up with an experienced mentor, you can make an offer for a mentor for a limited time period (~2 weeks) so that mentors are not overly burdened. Currently, mentors consist primarily of [Core Team members](https://about.gitlab.com/core-team/). 

The first-time contributors can be identified after each release in the [First time contributors MRs dashboard](https://gitlab.biterg.io/app/kibana#/dashboard/df97f810-f397-11e8-8fe1-b354a33b38be).

### Working with the Core Team
There will be a monthly call with members of the [Core Team](https://about.gitlab.com/core-team/) using the Zoom conferencing tool and meetings will also be recorded. Since Core Team members are spread out in different time zones, meeting times will be rotated.   

[Service Desk](https://gitlab.com/gitlab-core-team/general/issues/service_desk) will be used as a communication tool for Core Team members. Anyone can open issues in the Service Desk or send an email to `incoming+gitlab-core-team/general@incoming.gitlab.com`  Core Team members who signed NDAs will also have access to GitLab Slack channels.  

### For contributors to the GitLab Enterprise Edition (EE)

In order for community contributors to contribute to the [GitLab Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee), they will need a license for EE.  If they don't already have a license, they can get a [free trial for 30 days](https://about.gitlab.com/free-trial/).  If the contributor is not able to complete their work in 30 days, issue a new EE license for a 3-month period for a limited number of users. If the contributor wants to continue contributing to EE beyond the 3 months (and there has been contributions during the 3-month period), we can renew the license for another 3-month period.  

In order to request license keys for EE, you will need to get permission to login to dev.gitlab.org by creating an [access request issue](https://gitlab.com/gitlab-com/access-requests/issues). Details on requesting license keys can be found at [Working on GitLab EE](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee) section of the developer onboarding handbook.

### For contributors who run out of CI minutes

Community members may run out monthly CI minutes as they are working on an MR. Community members can send a request to reset their CI minutes to `contributors@gitlab.com` in order to continue their work. After reviewing the request, a Community Relations team member will file an issue with the [GitLab.com Support Team](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=pipeline_reset_request) using the `pipeline_reset_request` template. Information on the community member name, username, and link to the MR that requires additional CI minutes will need to be provided in the issue.    

### Contributor License Agreement (CLA) for contributions to GitLab Enterprise Edition (EE)
Contribution to the [GitLab Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee) requires accepting a [Contributor License Agreement(CLA)](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/CONTRIBUTING.md#contributor-license-agreement). In order to make the process as developer-friendly as possible, we do not require any paperwork and consider the act of submitting code via Merge Request as an agreement to [individual](https://gitlab.com/gitlab-org/gitlab-ee/blob/1bfac68311180a201001f1cd0b66833910dd9684/doc/legal/individual_contributor_license_agreement.md) or [corporate](https://gitlab.com/gitlab-org/gitlab-ee/blob/1bfac68311180a201001f1cd0b66833910dd9684/doc/legal/corporate_contributor_license_agreement.md) CLAs. 

The CLA only applies to GitLab EE, while other GitLab projects will continue to be licensed under MIT and simply require signing off on the [Developer Certificate of Origin (DCO)](#developer-certificate-of-origin-dco).  

### Developer Certificate of Origin (DCO)

Code contribution to GitLab projects with the exception of GitLab Enterprise Edition (EE) requires signing off on the [Developer Certificate of Origin (DCO)](https://gitlab.com/gitlab-org/dco/blob/master/README.md). 

In line with our value that [everyone can contribute](https://about.gitlab.com/company/strategy/#mission) to GitLab, we strive to make our process as developer-friendly and frictionless as possible. As such, we consider the act of contributing to the code by submitting a Merge Request as the "Sign off" or agreement to the certifications and terms of the DCO and MIT license. No further action is required.

Check out this [issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/43293) for more details.

## Community outreach

### Contributor blog post series

Goal is to publish a regular blog post featuring contributors from the community.  The format will be a [casual Q&A with a community member](https://about.gitlab.com/2018/08/08/contributor-post-vitaliy/) and will be posted on the [GitLab blog page](https://about.gitlab.com/blog/).

When developing a blog post, follow the [blog guidelines](/handbook/marketing/blog/).

### Following up on inactive merge requests

Periodically, there should be a review of community MR's that has not had a meaningful activity for an extended period (e.g. >3 months).  A good place to start would be to search for all MR's with a label `Community contribution` and a milestone `No milestone`.  A follow-up should be made with either the community member or MR coaches to ask if there's a plan to continue work on the MR or if MR should be closed.  

## Recognition for contributors

### Most Valuable Person (MVP)

Every release, a contributor is selected as a Most Valuable Person (MVP) for significant contribution(s) for that release. Suggestions/discussions for the MVP take place in the [#release-post Slack channel](https://gitlab.slack.com/messages/release-post). GitLab team-members and [Core Team members](https://about.gitlab.com/community/core-team/) are all encouraged to participate in the MVP discussion.  Based on the discussion in the Slack channel, the release post author for each release will make a decision on the MVP. See the [release post section](http://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) for more details on the MVP selection process. 

After each release, MVPs are added to the [GitLab Hall of Fame](https://about.gitlab.com/community/mvp/). 

### Top Annual Contributors

In order to recognize regular contributors, the list of top contributors for each calendar year will be published in the [Top Annual Contributors page](https://about.gitlab.com/community/top-annual-contributors/). There will be three categories of top contributors: 

*  SuperStar: more than 75 MRs merged
*  Star: between 11 and 75 MRs merged
*  Enthusiast: between 5 and 10 MRs merged

Customized GitLab merchandise will be created for these contributors. 

## Key Performance Indicators

### Wider community contributions per milestone

<iframe src="https://gitlab.biterg.io/app/kibana#/dashboard/465b66f0-882a-11e9-b37c-9d3431060b53?embed=true&_g=(refreshInterval%3A(display%3AOff%2Cpause%3A!f%2Cvalue%3A0)%2Ctime%3A(from%3Anow-10y%2Cmode%3Arelative%2Cto%3Anow))" height="600" width="800"></iframe><br />

Number of merge requests (MRs) merged from the wider GitLab community for each milestone.

To count as a contribution:

- The MR must have been merged
- The MR must have the [`Community contribution`](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) label
- The MR must have a GitLab milestone set (e.g `11.11`, `12.0`, etc.)
- The MR is against one of the [monitored `gitlab-org` group projects](https://gitlab.com/Bitergia/c/gitlab/sources/blob/master/projects.json)

Notes:
- Wider community metrics are more reliable after 2015 when the `Community contribution` label was created.
- There is also ongoing work to add this metric to the [Contributors Periscope dashboard](https://app.periscopedata.com/login?controller=welcome&dashboard_id=406359&site_host=gitlab).

## Contributor metrics

Note: this is currently a working list of all locations where we can currently gather contributor metrics. It is not *yet* the final set of metrics we will be using to monitor the success of the contributor program with.

### Bitergia dashboard
The [Bitergia dashboard](https://gitlab.biterg.io) is public and anyone can use the dashboard to view/filter/export/analyze the data. A good place to start is the [Merged Community MRs dashboard](https://gitlab.biterg.io/app/kibana#/dashboard/f88a8d00-f36b-11e8-8fe1-b354a33b38be) as it includes information that most people are looking for such as merged community MRs, number of contributors (e.g. yearly), top contributors, merged MRs per milestone, etc. You can filter the dashboard data per milestone and repository (e.g. CE vs. EE).        

There are a number of other custom dashboards also available and to see the full list, click on `dashboard` on the upperleft (next to the Bitergia logo) and then select the dashboard link from the list. To learn more about using the Bitergia dashboard, you can view recordings of Bitergia training at the [Bitergia training livestream channel](https://www.youtube.com/playlist?list=PL-gGdYiFOp7_9ij_wNLKyHgZsyhAY42rv).   

Some administrative features (e.g. getting a short URL, creating a new dashboard) require a login, and the login information is available in the Team Vault on 1Password.  

### Quality dashboard
The Quality team publishes a [monthly dashboard](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit#gid=319422041) with the data update on the 4th Monday of each month.  The `Community MRs per release` tab shows merged MRs from the wider community.  

### GitLab.com
You can also directly query data from `Merge Requests` pages for projects (e.g. CE, EE, Gitter, Omnibus, Shell, etc.) on gitlab.com and apply appropriate filters for milestone, labels, etc. Some of the examples are listed in the metrics table below.  

### Metrics table

NOTE: "GitLab team-members included" column
- Yes (✓) means the metric includes contributions or contributors that contain both GitLab team-members and the wider community
- No (✖) means the metric includes contributions or contributors that contain only the wider community

| METRIC | TIME SERIES | PROJECT | DATA SOURCE | GITLABBERS INCLUDED? | NOTES |
| - | - | - | - | - | - |
| <multiple> | Release | GitLab CE/EE | [Bitergia dashboard](https://gitlab.biterg.io) | ✖ | - |
| No. of merged contributions | Release | GitLab CE | [Quality dashboard](https://docs.google.com/spreadsheets/d/1sdppNaGYh1TkhOFdDkSz20YVhc9z9YKtusW-0pmBOtQ/edit#gid=319422041) | ✖ | To be phased out in favour of the Bitergia Dashboard |
| No. of contributors | All time | GitLab CE | [GitLab contributors app](https://contributors.gitlab.com) | ✓ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab organization | [Community contribution label](https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |
| No. of WIP open contributions | All time | GitLab CE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&wip=no&label_name[]=Community%20contribution) | ✖ | - |
| No. of merged contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=merged&label_name[]=Community%20contribution) | ✖ | - |
| No. of open contributions | All time | GitLab EE | [Community contribution label](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20contribution) | ✖ | - |

## Projects

The list of GitLab projects that we track contributions for is maintained in [Bitergia's GitLab projects file](https://gitlab.com/Bitergia/c/gitlab/sources/blob/master/projects.json).  