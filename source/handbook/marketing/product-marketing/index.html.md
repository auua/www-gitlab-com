---
layout: markdown_page
title: "Product Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What does product marketing do at GitLab?

Product marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Product marketing enables other GitLab teams such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Product marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

Responsibilities of the product marketing team at GitLab include:
- [Product messaging](/handbook/marketing/product-marketing/messaging/)
- [GitLab positioning](/handbook/positioning-faq/)
- [Partner marketing](#partner-marketing/)
- [Channel marketing](#channel-marketing/)
- [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
- [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)
- [Demos](/handbook/marketing/product-marketing/demo/)
- [Competitive intelligence](/handbook/marketing/product-marketing/competitive/)
- [Sales enablement](/handbook/marketing/product-marketing/enablement/)
- [Sales resources](/handbook/marketing/product-marketing/sales-resources/)
- [GitLab tiers](/handbook/marketing/product-marketing/tiers/)
- [Defining GitLab roles and personas](/handbook/marketing/product-marketing/roles-personas/)
- [Market segmentation - Industry verticals](/handbook/marketing/product-marketing/market-segmentation/)
- [Hidden IT Groups](/handbook/marketing/product-marketing/it-groups/)
- [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/product-marketing/getting-started/101/)
- [Getting Started - GitLab 102 - Working Remotely](/handbook/marketing/product-marketing/getting-started/102/)
- [PMM Metrics and KPIs](/handbook/marketing/product-marketing/pmm-metrics/))

## Which product marketing manager should I contact?

- Listed below are areas of responsibility within the product marketing team:

  - [Ashish](/company/team/#kuthiala), Director PMM
  - [John](/company/team/#j_jeremiah), PMM for [Dev Product Section](/handbook/product/categories/#dev-section) & Pub Sector
  - [William](/company/team/#thewilliamchia), PMM for [CI/CD](/handbook/product/categories/#cicd-section) & [Ops](/handbook/product/categories/#ops-section) Product Sections
  - [Dan](/company/team/#dbgordon), Technical PMM (CI/CD & Ops), Demos, Competitive
  - [Tye](/company/team/), Technical PMM (Dev), Demos, Competitive
  - [Cindy](/company/team/#cblake2000), PMM for [Secure](/handbook/product/categories/#secure-section) & [Defend](/handbook/product/categories/#defend-section) Product Sections
  - [Joyce](/company/team/#joycetompsett), Analyst Relations Manager
  - [Tina](/company/team/#t_sturgis), PMM for Partner and Channel
  - [Kim](/company/team/#kimlock), Customer Reference Manager
  - [Clint](/company/team/#Clint_II), Technical PMM for Competitive Intelligence

## What is Product marketing team working on?
- View the [Product Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/397296?=&label_name[]=Product%20Marketing) to see what's currently in progress.
- To ask for PMM resources log an issue in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues) and label it with `Product marketing`.
- Direct and channel sales enablement tasks are tracked on the [Sales Enablement Issue Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name[]=Sales%20Enablement).
- To request sales enablement topics log an issue in the [marketing general project](https://gitlab.com/gitlab-com/marketing/general/issues) and label it with `Sales enablement`.
- If you need more immediate attention please send a message in the `#product-marketing` slack channel with an `@reply` to the [PMM responsible](#roles) or you can ping the team with `@pmm-team`.

## PM vs PMM
- **PM** - Product Management or Product Manager. The [product team](/handbook/product/) as a whole, or the specific person responsible for a product area.
- **PMM** - Product Marketing Management or Product Marketing Manager. The product marketing team as a whole, or the specific person responsible for a product marketing area.

## Release vs launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |


## Demos

[Demos](./demo/) help show the value GitLab can bring to customers. Go to the [Demo page](./demo/) to see what's available and get more info.

## Competitive intelligence

Go to the [Competitive intelligence page](/handbook/marketing/product-marketing/competitive).

## Sales enablement

Go to the [Sales Enablement page](/handbook/marketing/product-marketing/enablement/).

## Customer facing presentations

Marketing decks linked on this page are the latest approved decks from Product Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

### Company pitch presentation deck
The [Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/) contains the GitLab narrative and pitch.

### SDR presentation deck
The [SDR deck](https://docs.google.com/presentation/d/1679lQ7AG6zBjQ1dnb6Fwj5EqScI3rvaheIUZu1iV7OY/edit#slide=id.g42cc3032dd_1_1903) is a condensed version of the company pitch deck. It copies linked slides from the pitch deck (so they can stay in sync.) SDR Managers own responsibility for keeping the deck in sync.

This deck can be used on occasions where the SDRs feel they should or could prequalify a prospect before setting a discovery meeting with the SAL. This could for example be someone who isn't our typical target persona but who might have an interest in what we do.

### GitLab security capabilities pitch presentation deck
The [Security Deck](https://docs.google.com/presentation/d/1z4v6v_lP7BHCP2jfRJ9bK_XoUgQ9XW01X2ZhQcon8bY/edit#slide=id.g2823c3f9ca_0_9) introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.

**Company pitch deck change process**
Only the PMM team and CMO have write access to the marketing decks. Changes, comments, and additions should be made via this process:
1. Create a google slide deck with the slide(s) you want to update
2. Create an issue in the [marketing issue tracker](https://gitlab.com/gitlab-com/marketing/general/issues)
3. Add a reason for the proposed changes in the description
4. Label the issue with "Product Marketing"

Discussion takes place in the issue and revisions are made on the slide until it is ready to move to the marketing deck. When it is ready, a PMM will move the slide into the marketing deck and close the issue.


## Print collateral
The following list of print collateral is maintained for use at trade shows, prospect and customer visits.  The GitLab marketing pages should be downloaded as PDF documents if a print copy for any marketing purposes is needed.

- [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
- [How GitLab is Enterprise Class](/enterprise-class)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)


## Partner marketing

### Partner marketing objectives

- Promote existing partnerships to be at top-of-mind for developers.
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

For a list of Strategic Partners, search for "Partnerships" on the Google Drive.

### Partner marketing activation

Our partner activation framework consists of a series of action items within a high-level issue. When a strategic partnership is signed, Product Marketing will choose the issue template called [partner-activation](https://gitlab.com/gitlab-com/marketing/general/issues/new) which will trigger notification for all involved in partner activation activities.

For each action item within this issue, a separate issue should be created by the assignee who is responsible for that action item within the allocated timeframe.

The partner should be included on the high level issue so they can see the planned activities and can contribute.

### Partner newsletter feature

In line with the objective of "Promote existing partnerships to be at top-of-mind for developers", a regular feature in our fortnightly (8th & 22nd) newsletter will promote our partners to our target audience. This feature should be co-authored by the partner.

Possible content:

- Feature on new partner if signed
- Feature on existing partner if major update released
- Feature on existing partner highlighting benefits of partner product
- 1-minute video showcasing the integration as a reference
- Blog post from partner
- Feature on how existing customer uses GitLab and partner

Suggested format:

- Length: Couple of paragraphs
- Links: GitLab integration/partner page & partner website

Creation:

Email potential partner with case for creating content/blog post which will feature in our newsletter. Also request that they include the content in their own newsletter.

Create separate issue for blog post in www-gitlab-com project with blog post label and assign to Content Marketing with the following information:
- A summary describing the partnership
- Any specific goals for the blog post (e.g. must link to this, mention this, do not do this...)
- Contacts from both sides for people involved in the partnership
- Any marketing pages or material created from both sides which we can use in the post
- Deadlines for the post (internal draft, partner draft review and publication)

After publication: Send to partner a summary of the click-throughs to their website, registration for webinar etc.

## Channel marketing

### Channel marketing objectives

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote [Reseller program](/resellers/program/) to recruit new resellers

### Reseller funds allocation determination

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the Google Drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLab, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.

## Speaker Abstracts

To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events.
