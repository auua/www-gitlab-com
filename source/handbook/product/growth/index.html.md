---
layout: markdown_page
title: Growth
---

## Overview & Philosophy

Growth is responsible for scaling GitLab usage by connecting users to the
existing value that GitLab already delivers. This is in contrast to the other
groups in our organization that are primarily concerned with creating value.
Growth analyzes the entire customer journey from acquisition of a customer, to
the flow across multiple GitLab features, and even reactivation of lost users.
Growth is commonly asking and finding out the answers to these types of
questions:

* Why are so many users signing up for GitLab but never logging in?
* How do we successfully attract new customers to GitLab?
* How do we successfully win back customers that once used and/or paid for GitLab but have canceled their subscription?
* What is the first action taken after logging in and is it the right one for maximum connection to value?
* How can we grow the amount of customers that use all stages of GitLab?

## Growth Strategy  

Growth is expected to always start with a hypothesis. The hypothesis should be
fully defined prior to execution in order to safeguard from "bad science," namely
data that may correlate but not have any causal relationship. After a hypothesis has been
defined, Growth relies on two primary constructs to make decisions: analysis of data, or
what people do in the application, and qualitative research, in essence the
“why” behind the data.

Growth should focus on metrics that are important at the company level, while also considering
company strategy. We have broken down the primary metrics of interest in the various Growth functions below.
Other metrics of interest may surface during Growth exploration, and they may become important to the company,
but this is a byproduct of the process, rather than a goal.

### Growth Structure

Growth will operate in small groups aligned around the four growth functions
listed below. Each group will consist of a product manager, engineering manager,
some full-stack engineers, a (shared) data analyst, and a (shared) Product Designer.
Each group member will have a functional reporting structure, in order to report
to a leader that understands their job function.

#### Working across stages

The hypotheses explored by Growth will span across groups and stages. How do other groups interact with Growth?

Growth does not own areas of the product, but finds ways to help users discover and unlock value throughout the GitLab
application. They will do a combination of shipping low-hanging fruit themselves, but will partner with other stages
on larger initiatives that need specialized knowledge or ongoing focus.

It is the responsibility of other stages to maintain and expand core value in their respective product areas; it's
Growth's mission to streamline delivery of that value to users. For example:

* [Manage](https://about.gitlab.com/handbook/product/categories/#manage-stage) is responsible for user registration and
management. This stage maintains a long-term vision on how users are created and maintained in GitLab, with a roadmap that
includes other categories and priorities.
* Growth may identify user registration as an opportunity to further the group's [priorities](#growth-priorities). Growth's
engineering team may ship smaller improvements independently of Manage (such as [soft email confirmation](https://gitlab.com/gitlab-org/growth/issues/72)), but may need to partner with Manage on larger efforts like a completely [new onboarding](https://gitlab.com/gitlab-org/gitlab-ce/issues/57832) experience.

### Why Invest in a Growth Team in 2019?

We believe we have found product-market fit by providing a single application
for the entire DevOps lifecycle, highlighting the value created when teams don’t
have to spend time integrating disparate tools. Now that product-market fit has
been achieved, we must do a better job of connecting our value to our customers.
Therefore, a Growth focus is required. Additionally, we are beginning to put
more strategic focus on the growth of GitLab.com, which is a different user
signup and activation process than self-managed instances, which typically
involves direct sales.

## Growth Groups

### Composition

Each group will consist of:

1. Product Manager
1. 2 engineers
1. Data analyst
1. UX person
1. Product marketing manager (maybe shared between groups)

### Activation

Primary metric: Acquired and activated users, last 30 days

### Adoption

Primary metric: [Stage Monthly Active Users (SMAU)](/handbook/product/growth/#smau)

### Upsell

Primary metric: Percent (%) of users who went to a (higher) subscription

### Retention

Primary metric: Retention, including reactivation

### Telemetry

Responsible for product usage data, including proper instrumentation, working with BizOps and Engineering to ensure product usage data is stored correctly, and working across the Product team to ensure product usage data is analyzed and visualized in a way that can drive product and growth decisions.

### Fulfillment

Responsible for licensing & billing workflows, which have a huge impact on renewal rates, upgrade rates, and customer satisfaction, and represent important levers for the Growth team.

## Funnel

1. Visit
1. Acquisition (Signup)
1. Activation (Key usage)
1. Adoption (Increased stage usage)
1. Conversion
1. Upsell
1. Churn (!Retention)
1. Reactivation

## Metrics

### SMAU

SMAU stands for Stage Monthly Active Users.
The growth group is responsible for increasing these.

SMAU can be increased in multiple ways:

1. Increase usage pings
1. Increase new users
1. Increase retention of users
1. Increase reactivation of users
1. Increase number of stages per user
1. Increase the number of stages

Each of these is worthwhile to increase for their own sake:

1. More insight in how our product is used
1. More people introduced to the product
1. Happier users of the product
1. Winning people back
1. When people use more stages they are less likely to churn and more likely to buy
1. A more extensive application that addresses more use cases

Each of these has different ways of increasing it:

1. Cloud License Management (Sync) and registration for dependency scanning
1. Request an account when you can't self-signup, instead of always having to ask the admin make it for you.
1. Fix problems that churned users report.
1. Email churned users when features are introduced that they requested before churning.
1. In-product hints at relevant times to use a new stage.
1. Add a network security stage.

### SMAU factor

Per organization we can determine a SMAU factor to determine adoption:

SMAU factor = SMAU / Potential users * GitLab Stages

## Growth Inspiration

While the Growth function is becoming more common in late stage, well funded
software companies, it is still a relatively nascent discipline. At GitLab, we
seek to derive best practices from the industry and adapting them to work with
our culture. Here are a few articles we’ve found helpful as we are formulating a
Growth strategy and vision:

* [What Are Growth Teams For, and What Do They Work On?](https://news.greylock.com/what-are-growth-teams-for-and-what-do-they-work-on-a339d0c0dee3)
* [Sustainable Product Growth](https://www.sequoiacap.com/article/sustainable-product-growth)
