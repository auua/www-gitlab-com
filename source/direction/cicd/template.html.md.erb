---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

## Overview

The CI/CD section focuses on the code build/verification ([Verify](/direction/verify)), packaging/distribution
([Package](/direction/package)), and delivery ([Release](/direction/release)) stages of the
[DevOps Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). Each of these areas has their own strategy
page with upcoming features, north star directions, and more. This page ties them together via important concepts
that unify the direction across all of these areas. In addition to the core CI/CD elements, the 
[mobile use case](/direction/mobile) is very much related and will be of interest to you if you are interested
in building and releasing mobile applications using GitLab.

If you'd like to discuss this vision directly with the product director for CI/CD, 
feel free to reach out to Jason Lenny via [e-mail](mailto:jason@gitlab.com) or on [Twitter](https://twitter.com/j4lenn).
Your contribution is more than welcome.

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_11_11.png "Pipeline Infographic")

## Theme: Compliance as Code

Many in the industry talk about compliance as code (i.e., methods that ensure the correct regulatory or company
compliance requirements) as a state where requirements are fulfilled with zero-touch on the path to production.
We believe that [compliance without friction](https://about.gitlab.com/solutions/compliance/) is a crucial
solution for large enterprises and those subject to complex regulation, and that GitLab is well suited to
provide a comprehensive solution.

At the moment we are only scratching the surface for what customers need to achieve minimally viable compliance. 
However, our vision for a single application - integrating not only Secure, Verify, Package and Release but the entire
DevOps lifecycle - will uniquely position GitLab to have the ability to be the number one market leader in
built-in compliance, for users big or small. Beyond just providing places to plug-in your own compliance
process, we have a vision for compliance best-practices being delivered through GitLab.

The primary home for this theme is our [Release Governance](https://about.gitlab.com/direction/release/release_governance/)
category.

## Theme: Progressive Delivery

Put simply, Progressive Delivery is a core set of ideas and emerging best practices, oriented around
being able to control and monitor deployments in stages over time and in an automated and safe
way. GitLab is at a unique advantage when it comes to enabling this because, until now, Continuous
Delivery solutions have often required integrating various point products to provide each of the
individual capabilities needed. Here at GitLab, we're going to be implementing and
integrating these features within our single application, with Progressive Delivery in mind from the start.

Additional articles on Progressive Delivery can be found on the [LaunchDarkly blog](https://launchdarkly.com/blog/progressive-delivery-a-history-condensed/),
[RedMonk](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/) and 
[The New Stack](https://thenewstack.io/the-rise-of-progressive-delivery-for-systems-resilience/).

[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/) and [Continuous Delivery](https://about.gitlab.com/direction/release/continuous_delivery/) are
our core categories for Progressive Delivery, but there are additional ones that will be important
in providing our comprehensive solution:

- [Feature Flags](https://about.gitlab.com/direction/release/feature_flags/) for targeted rollouts
- [Review Apps](https://about.gitlab.com/direction/release/review_apps/) for on-demand validation environments
- [Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89) for behavior analysis across progressive deployments

We truly believe Progressive Delivery is the future of software delivery, and we plan to deliver an MVC
solution soon: [gitlab-org#1198](https://gitlab.com/groups/gitlab-org/-/epics/1198)

## Theme: Powerful, Integrated Primitives

Our vision for CI/CD remains aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book.
The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers
to getting things done has stood the test of time. This builds on the individual [Verify](/direction/verify), [Package](/direction/package), 
and [Release](/direction/release) stage direction pages.

We think there are a few simple but powerful primitives to add to GitLab CI/CD which will allow us to achieve this vision.
Each of these works independently and solves problems from simple to advanced, but truly become powerful when used in concert.
Because everything in CI/CD begins with the pipeline, that's where we focus our attention.

### Next Up

There are a few key places to get started, so we've scheduled the following items to start moving forward our vision here.

- **[Multiple Pipelines in .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972)**: The first important step for us to take is to
  implement multiple, parallel pipelines within a single `.gitlab-ci.yml`. This feature enables much of what's to follow by
  creating a unique code-execution path for different parts of a single pipeline. This even enables a simple form of namespacing:
  because each sub-pipeline will run within its own pipeline context, parallel execution paths within a single project's master
  pipeline each operate within their own execution context for includes and there is no risk of cross-contamination/conflicts.
  Sub-pipelines would be able to be triggered based on any `only`/`except` criteria, solving an important use case for monorepos
  (i.e., only run sub-pipeline _x_ when there are changes in repository path _y_.) This isn't quite a DAG (see below), but it
  solves most use cases that requestors of a DAG were looking for.

- **[Workspaces](https://gitlab.com/gitlab-org/gitlab-ce/issues/47062)**: Persisting untracked files between jobs in a pipeline 
  (and even publishing them for later download) is done using a mix of caching and artifact functionality in GitLab. Neither
  is really ideal for the purpose - cache is not guaranteed to be available, and artifacts are more heavyweight than they need to
  be for this usage. Once we've implemented multiple pipelines, workspaces become much simpler to implement. By leveraging
  the fact that each code path within a multi-pipeline runs as its own pipeline, we can make the configuration very simple..
  there's no need to analyze the dependency graph to determine what should be included, we can simply allow pipelines
  to generate workspaces for themselves that exist purely within the sub-pipeline context.

  For use cases in the data sciences where there may be a sub-pipeline for each data source and a series of transformations that
  run on them, sub-pipeline workspaces also provide a very natural way to execute sequential transformations that run independently
  and have a clear set of intermediary objects that need to move from job to job.

### Researching

- **[Directed Acyclic Graph (DAG)](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063)**: There's a simple graph model in
  GitLab today, but this works by assuming everything in a single stage runs in parallel, and every job in the following
  stage must wait for all the jobs in the previous stage to complete. Multiple pipeline plus workspaces solves a lot of use
  cases where independent parallelization is needed, but sometimes you have an incredibly complex relationship between jobs
  and just want to define the relationships and let GitLab figure out how to execute it in the most efficient way possible.
  Under this mode of operation, GitLab would look at the `needs:` relationships between all the jobs in your pipeline, and
  execute them as efficiently as possible. While research here continues, for the moment we want to see how many use cases
  are solved with sub-pipelines and workspaces alone.

- **Namespaces for Includes**: More formal namespaces for includes are possible, but similar to the DAG we're waiting to see
  how users of sub-projects utilize that feature to determine what's really needed next. There is still the possibility that,
  even within a single sub-pipeline's context, that includes and implementation will become complex enough to require this.
  This could also become more important if sharing of includes becomes more commonplace. For now, though, we're in a wait-and-see mode.

- **Pipeline Entity Model**: Pipelines today can internally track a global success/failure status and generate artifacts,
  but have no way to really reflect on those and provide more advanced behaviors based on the internal state of the pipeline. By
  adding typed artifacts (a container image, a .json file, a gemfile) and internal state (the yaml used to run the job, status) as
  something available for introspection at runtime or to be used as a dependency in the DAG, we open the door to incredibly advanced
  cross or per-project pipeline behaviors based on generation of actual dependencies. As an additional benefit, this collection of
  entities within the pipeline itself becomes the bill of materials, collecting auditing and compliance evidence for the release as it runs.

- **Simplify Configuration**: In general, configuration syntax can be unwieldy. We have issues like [gitlab-ce#19199](https://gitlab.com/gitlab-org/gitlab-ce/issues/19199)
  that will allow for a cartesian product of configurations to be defined, without having to define each job in the matrix.
  This could be used for environments, architectures or platforms, or any other situation where you need to iterate a set of
  jobs over a few different configuration values. Related to this is a potential integration with technologies like JSonnet
  ([gitlab-ce#62456](https://gitlab.com/gitlab-org/gitlab-ce/issues/62456)) which allows for an easier to use, templated way
  to implement configuration. Another step even beyond this could be [gitlab-ce#45828](https://gitlab.com/gitlab-org/gitlab-ce/issues/45828)
  which will allow for custom code to be inserted that would then generate your `.gitlab-ci.yml`. This would be incredibly
  powerful, but could also be quite error prone so we'd need to balance that carefully.

- We also eventually plan to enable auto-inclusion of configuration YAML from subfolders, making it even easier to generate sub-pipelines
  as needed.

If you're interested in learning more, check out this [deep dive video](https://youtube.com/embed/FURkvXiiJek) where we discuss in detail how these ideas related to each other, or [this one](https://youtu.be/XxFX2FOp83A) in which we discuss sequencing options.

## What's Next for CI/CD

<%= direction %>
