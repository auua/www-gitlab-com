---
layout: markdown_page
title: "Reseller Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts 

## OnDemand Webcasts 

### GitLab 101
[Watch](/webcast/reseller/gitlab101-for-resellers/) to learn more about GitLab, the first single application to manage all stages of the DevOps lifecycle.

### Introduction to GitLab with GitBasics    
[Watch](/webcast/reseller/git-basics) a walkthrough of the GitLab user interface, its features and functionality and a demonstration of the GitLab flow for development. Also learn the basics of using Git and how to use it with GitLab. How to configure a local workspace, clone & pull from the GitLab, make local changes using the GitLab flow, commit them and finally push them back to the GitLab.    

### Introduction to GitLab's integrated CI/CD   
[Watch](/webcast/reseller/cicd-ondemand) an introductory course to using GitLab CI/CD, covering the .gitlab-ci.yml file focusing on some commonly used features and functionality and the GitLab Runner.   

### Product Tiers  
[Watch](/webcast/reseller/product-tiers) to learn more about GitLab product tiers and maturity levels.

### Selling the value of GitLab Ultimate
[Watch](/webcast/reseller/deep-dive/) to learn how you can help your enterprise customers solve their delivery challenges with GitLab Ultimate.

### Customer case studies
[Watch](/webcast/reseller/customer-case-studies/) to hear how three customers leverage GitLab to gain a competitive advantage.

### GitLab's Security features - part 1
[Watch](/webcast/reseller/security-features/) to learn about GitLab's Security features and why they are important to your customers.

### GitLab's Security features - part 2
[Watch](/webcast/reseller/security-features-part-2/) to learn about GitLab's Security features and why they are important to your customers.

### GitLab's security message
[Watch](/webcast/reseller/security-message) to learn how to position GitLab’s security message and win business.

### GitLab FY2020 Virtual SKO
[Watch](/webcast/reseller/FY2020-reseller-sko)  the first annual Sales Kick Off for GitLab Resellers.

### How to run a successful GitLab meetup
[Watch](/webcast/reseller/gitlabmeetup-tips) to learn how to organize and run a successful GitLab meet up.
