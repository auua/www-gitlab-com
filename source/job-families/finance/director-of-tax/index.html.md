---
layout: job_family_page
title: "Director of Tax"
---

This position is responsible for GitLab’s overall tax strategy including all components
of compliance, tax planning and accounting for income taxes. The successful candidate will
work with advisors, service providers, and other tax professionals in carrying out his or her responsibilities.
This person will regularly report to the Company’s Board of Directors and/or Committees
of the Board to update the members on the Company’s tax strategy, practices and exposures.


## Responsibilities

* Primary responsibility for establishing GitLab’s international tax strategy.
* Responsible for completion of national and/local state income tax returns including all related analysis and support.
* Oversight of the tax returns for the Company’s in all operational jurisdictions.
* Responsible for VAT, Sales, Use and Property tax functions.
* Responsible for audits of international, federal and state income tax and state and local filings.
* Responsible for accounting for income taxes (ASC 740) in the US and International subsidiaries.
* Coordination of accounting for income taxes for international subsidiaries with third party vendors.
* Responsible for transfer pricing and management fee arrangements.
* Responsible for building and growing team of professional and paraprofessional staff.
* Liaison with operating management on tax issues and accounting staff on tax accounting issues.
* Ensuring that appropriate internal controls are in place over accounting for income taxes.
* Departmental liaison with IT staff on all technical matters relating to tax applications.
* Ensure the Company implements and maintains controls compliant with Sarbanes-Oxley.

## Requirements

* Bachelor’s Degree (B.S.) in Accounting. Master’s Degree in Business Taxation preferred.
* JD and/or CPA preferred.
* 10 years of related experience with at least 5 years in a public accounting firm.
* Experience with Software and/or SAAS in a high growth environment.
* Must have demonstrated a hands-on approach and success in working in a team-based environment
* International tax experience required with preference for candidates based in the Netherlands, UK or Ireland.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below.
Please keep in mind that candidates can be declined from the position at any stage of the process.
To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our CFO
* Candidates will then be invited to schedule an interview with our Director of Legal
* Next, candidates will be invited to schedule an interview with our External Tax Advisor
* Finally, candidates will have a 30min call with either our CEO or a GitLab Audit Committee Member

Additional details about our process can be found on our [hiring page](handbook/hiring).
